<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */

$paradise_page_layouts = apply_filters( 'paradise_fnc_sidebars_others_configs', null );


if( isset($paradise_page_layouts['sidebars']) ): $sidebars = $paradise_page_layouts['sidebars']; 
?> 
	<?php if ( $sidebars['left']['show'] ) : ?>
	<div class="<?php echo esc_attr($sidebars['left']['class']) ;?> <?php  if( paradise_fnc_theme_options('stickysidebar') ) : ?> sidebar-wrapper<?php endif; ?>">
	  <aside class="sidebar sidebar-left" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
	   	<?php dynamic_sidebar( $sidebars['left']['sidebar'] ); ?>
	  </aside>
	</div>
	<?php endif; ?>
 	
 	<?php if ( $sidebars['right']['show'] ) : ?>
	<div class="<?php echo esc_attr($sidebars['right']['class']) ;?> <?php  if( paradise_fnc_theme_options('stickysidebar') ) : ?> sidebar-wrapper<?php endif; ?>">
	  <aside class="sidebar sidebar-right" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
	   	<?php dynamic_sidebar( $sidebars['right']['sidebar'] ); ?>
	  </aside>
	</div>
	<?php endif; ?>
<?php endif; ?> 


<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$layout = $style;
?>

<?php if( $layout === 'style-2' ){ ?>
<div class="pbr-contact-information style-2 clearfix <?php echo esc_attr($el_class); ?>">
   <div class="widget-inner">   
      <?php if($address){ ?>
      <div class="address item clearfix"><i class="icon fa fa-home"></i><span><?php echo esc_html($address) ?></span></div>
      <?php } ?>  
      <?php if($phone){ ?>
         <div class="phone item clearfix"><i class="icon fa fa-tablet"></i><span><?php echo esc_html($phone) ?></span></div>
      <?php } ?> 
      <?php if($fax){ ?>
         <div class="phone item clearfix"><i class="icon fa fa-fax"></i><span><?php echo esc_html($fax) ?></span></div>
      <?php } ?> 
      <?php if($email){ ?>
         <div class="email item clearfix"><i class="icon fa fa-envelope"></i><span><?php echo esc_html($email) ?></span></div>
      <?php } ?> 
   </div>    
</div>

<?php } else { ?>
<div class="pbr-contact-information clearfix <?php echo esc_attr($el_class); ?>">
   <div class="widget-inner">   
      <?php if($address){ ?>
      <div class="address item"><p class="clearfix"><i class="icon fa fa-map-marker text-primary"></i><label><?php echo esc_html__( 'Address', 'paradise' ) ?></label></p><span><?php echo esc_html($address) ?></span></div>
      <?php } ?>  
      <?php if($phone){ ?>
         <div class="phone item"><p class="clearfix"><i class="icon fa fa-phone text-primary"></i><label><?php echo esc_html__( 'Hot line', 'paradise' ) ?></label></p><span><?php echo esc_html($phone) ?></span></div>
      <?php } ?> 
      <?php if($fax){ ?>
         <div class="phone item"><p class="clearfix"><i class="icon fa fa-fax text-primary"></i><label><?php echo esc_html__( 'Fax', 'paradise' ) ?></label></p><span><?php echo esc_html($fax) ?></span></div>
      <?php } ?> 
      <?php if($email){ ?>
         <div class="email item"><p class="clearfix"><i class="icon fa fa-envelope-o text-primary"></i><label><?php echo esc_html__( 'Email', 'paradise' ) ?></label></p><span><?php echo esc_html($email) ?></span></div>
      <?php } ?> 
   </div>    
</div>
<?php } ?>
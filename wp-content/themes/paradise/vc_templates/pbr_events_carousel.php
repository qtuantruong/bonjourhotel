<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$loop = paradise_fnc_event_query($orderby, $number);
$_count = 0;
$_id = paradise_fnc_makeid();
$_total = $loop->post_counts;
?>

<section class="widget widget-events-carousel <?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php if ($title!='') { ?>
        <h3 class="widget-title visual-title">
            <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
	<?php if ( $loop->have_posts() ) : ?>
		<div id="carousel-<?php echo esc_attr($_id); ?>" class="owl-carousel-play" data-ride="owlcarousel">
	        <?php if ($number > $columns_count ) { ?>
	            <div class="carousel-controls carousel-controls-v3">
	                <a class="left carousel-control carousel-md" href="#" data-slide="prev">
	                    <span class="fa fa-angle-left"></span>
	                </a>
	                <a class="right carousel-control carousel-md" href="#" data-slide="next">
	                    <span class="fa fa-angle-right"></span>
	                </a>
	            </div> 
	        <?php } ?>
	        <div class="owl-carousel" data-slide="<?php echo esc_attr($columns_count); ?>" data-pagination="true" data-navigation="true">
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					<div class="item">
						<?php tribe_get_template_part( 'list/single', 'carousel' ) ?>
					</div>
				<?php $_count++; endwhile; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	<?php endif; ?>
</section>
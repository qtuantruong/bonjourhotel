<?php 
$layout = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );

extract( $atts );

if(is_front_page()){
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
}
else{
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}

$query = paradise_opalhotel_room_query($orderby, $number, $paged);

$_count = 0;
?>
<?php if( $query->have_posts() ) : ?>
<div class="opalhotel-list-rooms <?php echo (($el_class!='')?' '.$el_class:''); ?>">
	<div class="inner">
		<div class="room-list-item">
			
			<?php $i=0; while (  $query->have_posts() ) :  $query->the_post(); $_count++; ?>
				<?php opalhotel_get_template_part( 'content-room', 'list' ); ?>
				
			<?php endwhile; ?>
		</div>
		
	</div>

	<div class="space-margin-bottom-50p">
		<?php 
			if($pagination == 1){
				paradise_fnc_pagination_nav( $number, $query->found_posts, $query->max_num_pages );
			}
		?>
	</div>

</div>
<?php else : ?>
<div><?php esc_html_e( 'No found record', 'paradise' ); ?></div>
<?php endif; ?>	
<?php 
wp_reset_query();
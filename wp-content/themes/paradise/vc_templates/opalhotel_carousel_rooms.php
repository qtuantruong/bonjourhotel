<?php 
$layout = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$query = paradise_opalhotel_room_query($orderby, $number);
$_id = paradise_fnc_makeid();
$_count = 0;
?>
<?php if( $query->have_posts() ) : ?>
<div class="opalhotel-carousel-rooms">
	<div class="inner">
		<div class="owl-carousel-play" data-ride="carousel">
			<?php   if( $query->post_count > $columns ) { ?>
				<div class="carousel-controls">
					<a href="#roomcarousel-<?php echo esc_attr($_id); ?>" data-slide="prev" class="left carousel-control carousel-xs">
						<i class="fa fa-chevron-left"></i>
					</a>
					<a href="#roolcarousel-<?php echo esc_attr($_id); ?>" data-slide="next" class="right carousel-control carousel-xs">
						<i class="fa fa-chevron-right"></i>
					</a>
				</div>
			<?php } ?>
			<div class="owl-carousel opalhotel-owl-content" data-slide="<?php echo esc_attr($columns); ?>"  data-singleItem="true" data-navigation="true" data-pagination="false">
				<?php $i=0; while (  $query->have_posts() ) :  $query->the_post(); $_count++; ?>
					
					<?php   echo '<div class="item">'; ?>
						<?php opalhotel_get_template_part( 'content-loop-room', $layout); ?>
					<?php  echo '</div>'; ?>
					
				<?php endwhile; ?>
			</div>
		</div> 

	</div>
</div>
<?php else : ?>
<div><?php esc_html_e( 'No found record', 'paradise' ); ?></div>
<?php endif; ?>	
<?php 
wp_reset_query();
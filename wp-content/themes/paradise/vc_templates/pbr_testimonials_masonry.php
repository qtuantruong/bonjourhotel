<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
	$columns = 2;
	$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
	extract( $atts );

	$_id = paradise_fnc_makeid();
	$_count = 0;
	$args = array(
		'post_type' => 'testimonial',
		'posts_per_page' => $number,
		'post_status' => 'publish',
	);

	$query = new WP_Query($args);
	wp_enqueue_script( 'wpo_isotope_js', get_template_directory_uri().'/js/isotope.pkgd.min.js', array( 'jquery' ) );
?>

<div class="widget-nostyle wpo-testimonial-masonry <?php echo esc_attr($el_class); ?>">
	<?php if($query->have_posts()){ ?>
		<?php if($title!=''){ ?>
			<h3 class="widget-title visual-title <?php echo esc_attr($size).' '.esc_attr($alignment); ?>">
				<span><?php echo trim($title); ?></span>
			</h3>
		<?php } ?>
 
		<div id="carousel-<?php echo esc_attr($_id); ?>" class="widget-content blog-masonry row">
			<?php  $_count=0; while($query->have_posts()):$query->the_post(); ?>
				<div class="testimonials item isotope-item col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<?php  
						get_template_part( 'vc_templates/testimonials/testimonials', 'v4' ); 
					?>
				</div>
				<?php $_count++; ?>
			<?php endwhile; ?>
		</div>
	<?php } ?>
</div>
<?php wp_reset_postdata(); ?>
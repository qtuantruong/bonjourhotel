<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<div class="widget pbr-opentime-widget <?php echo esc_attr($el_class); ?>">
    
    <div class="widget-content">

        <ul class="opentime-list">

            <?php
                $line_data = array();
                $items = (array) vc_param_group_parse_atts( $items );
                foreach ( $items as $data ) {
                    $new_line = $data;
                    $new_line['date'] = isset( $data['date'] ) ? $data['date'] : '';
                    $new_line['time'] = isset( $data['time'] ) ? $data['time'] : '';
                    $line_data[] = $new_line;
                }
            ?>

            <?php
                if($line_data): 
                $i = 1;
                foreach ($line_data as $key => $item):
            ?>
                <li class="opentime-plan clearfix">
                    <div class="pull-left">
                        <span class="openning-date"><?php echo esc_html($item['date']) ?></span>
                    </div>
                    <div class="pull-right">
                        <span class="openning-time"><?php echo esc_html($item['time']); ?></span>
                    </div> 
                  </li>
            <?php $i++; endforeach; endif; ?>  
        </ul>

    </div>
    
</div>
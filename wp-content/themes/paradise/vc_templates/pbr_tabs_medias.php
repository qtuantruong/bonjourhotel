<?php
/**
 * Shortcode attributes
 * @var $atts
 * @var $title
 * @var $number
 * @var $columns_count
 * @var $show_tab
 * @var $style
 * @var $filter
 * @var $title_align
 * @var $size
 * @var $el_class
 * @var $loop
 * @var $load_more
 * Shortcode class
 * @var $this WPBakeryShortCode_PBR_All_Products
 */
wp_enqueue_script( 'prettyphoto' );
wp_enqueue_style( 'prettyphoto' );

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$_id = paradise_fnc_makeid();
$_count = 0;

$args_v = array(
	'post_type' => 'video',
	'posts_per_page' => $number,
	'post_status' => 'publish',
);

$query_v = new WP_Query($args_v);

$args_g = array(
	'post_type' => 'gallery',
	'posts_per_page' => $number,
	'post_status' => 'publish',
);

$query_g = new WP_Query($args_g);

?>
	<div class="widget widget-medias-tabs">
		<div class="tabs-container tab-heading text-center clearfix tab-v8">
			
			<ul class="tabs-list nav nav-tabs">
				<li class="active"><a href="#tab1-1" data-toggle="tab">Photos</a></li>
				<li><a href="#tab1-2" data-toggle="tab">Videos</a></li>
			</ul>
		</div>
		<div class="widget-content tab-content">

			<div id="tab1-1" class="tab-pane active">
				<?php $_count=0; while($query_g->have_posts()):$query_g->the_post(); $_count++; global $post; ?>
				<?php 
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				?>	
					<?php if($_count % $columns == 1) echo '<div class="row">'; ?>
						<div class="item col-sm-<?php echo floor(12/$columns) ?> col-xs-4">
							<div class="image">
								<?php the_post_thumbnail('large'); ?>
								<div class="mask zoom-2"><a class="prettyphoto" href="<?php echo esc_url_raw($image[0]); ?>"></a></div>
							</div>
						</div>
					<?php if($_count % $columns == 0 || $_count == $query_g->found_posts) echo '</div>'; ?>
				<?php endwhile; ?>
			</div>

			<div id="tab1-2" class="tab-pane">
			
				<?php if($query_v->have_posts()){ ?>
					<?php $_count=0; while($query_v->have_posts()):$query_v->the_post(); $_count++; global $post; ?>
					<?php 
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
						$url = get_post_meta( get_the_ID(), 'paradise_video_url', true);
						$url = empty($url)? '#' : $url;
					?>	
						<?php if($_count % $columns == 1) echo '<div class="row">'; ?>
							<div class="item col-sm-<?php echo floor(12/$columns) ?> col-xs-4">
								<div class="image">
									<?php the_post_thumbnail('large'); ?>
									<div class="mask zoom-2"><a class="prettyphoto" href="<?php echo esc_url_raw($url); ?>"><i class="fa fa-play"></i></a></div>
								</div>
							</div>
						<?php if($_count % $columns == 0 || $_count == $query_v->found_posts) echo '</div>'; ?>

					<?php endwhile; ?>

				<?php } ?>

			</div>

		</div>
	</div>
<?php wp_reset_postdata();?>


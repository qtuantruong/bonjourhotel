<?php 
$layout = '';
global $page_link;
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$query = paradise_opalhotel_room_query($orderby, $number);

$page_link = $reservation_page ;

$_count = 0;
?>
<?php if( $query->have_posts() ) : ?>
<div class="opalhotel-overlap-rooms">
	<div class="inner">
		 
		<?php $i=0; while (  $query->have_posts() ) :  $query->the_post(); $_count++; ?>
			<?php if($_count % $columns == 1) echo '<div class="row">'; ?>
				<div class="col-sm-<?php echo floor(12/$columns) ?> col-xs-12">
					<?php if($style == 'style-2') { ?>
						<?php opalhotel_get_template_part( 'content-room', 'overlap-2'); ?>
					<?php } else { ?>
						<?php opalhotel_get_template_part( 'content-room', 'overlap'); ?>
					<?php } ?>
				</div>
			<?php if($_count % $columns == 0 || $_count == $query->found_posts) echo '</div>'; ?>
			
		<?php endwhile; ?>
		 
	</div>
</div>
<?php else : ?>
<div><?php esc_html_e( 'No found record', 'paradise' ); ?></div>
<?php endif; ?>	
<?php 
wp_reset_query();
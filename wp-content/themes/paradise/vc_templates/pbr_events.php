<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$loop = paradise_fnc_event_query($orderby, $number);
$_count = 0;
$_id = paradise_fnc_makeid();
$_total = $loop->post_counts;
?>

<section class="widget widget-events-list <?php echo (($el_class!='')?' '.$el_class:''); ?>">
    <?php if ($title!='') { ?>
        <h3 class="widget-title visual-title">
            <span><?php echo trim($title); ?></span>
        </h3>
    <?php } ?>
	<?php if ( $loop->have_posts() ) : ?>
		<div class="widget-content">
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="event-item">
					<?php if( isset($show_image) && $show_image ) { ?>
			        	<?php tribe_get_template_part( 'list/single', 'countdown-2' ) ?>
			        <?php } else { ?>
						<?php tribe_get_template_part( 'list/single', 'countdown' ) ?>
					<?php } ?>
				</div>
			<?php $_count++; endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	<?php endif; ?>
</section>
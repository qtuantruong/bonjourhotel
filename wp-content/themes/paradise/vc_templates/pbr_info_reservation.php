<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
?>

<div class="pbr-info-reservation">
	<div class="info-reservation-wrapper text-center">
		<figure>
			<?php $img = wp_get_attachment_image_src($photo,'full'); ?>
			<?php if( isset($img[0]) )  { ?>
				<img src="<?php echo esc_url( $img[0] );?>" alt="<?php echo esc_attr( $title ); ?>"  />
			<?php } ?>
		</figure>

		<h6><?php echo trim( $title ); ?></h6>
		<div class="reservation-phone">
			<?php if( $phone ){  ?>
				<a class="text-primary" href="<?php echo esc_url( $phone ); ?>"><?php echo esc_html($phone) ?></a>
			<?php } ?>
		</div>
		<div class="description">
			<?php echo esc_html($information); ?>
		</div>
	</div>
</div>

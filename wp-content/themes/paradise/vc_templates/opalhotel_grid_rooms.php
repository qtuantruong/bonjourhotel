<?php 
$layout = '';
global $page_link;
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

if(is_front_page()){
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
}
else{
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}

$query = paradise_opalhotel_room_query($orderby, $number, $paged);

$_count = 0;
?>
<?php if( $query->have_posts() ) : ?>
<div class="opalhotel-grid-rooms">
	<div class="inner">
		 
		<?php $i=0; while (  $query->have_posts() ) :  $query->the_post(); $_count++; ?>
			<?php if($_count % $columns == 1) echo '<div class="row">'; ?>
				<div class="col-sm-<?php echo floor(12/$columns) ?> col-xs-12">
					<?php opalhotel_get_template_part( 'content-loop-room', $layout); ?>
				</div>
			<?php if($_count % $columns == 0 || $_count == $query->found_posts) echo '</div>'; ?>
			
		<?php endwhile; ?>
		 
	</div>
	<div class="space-margin-bottom-50p">
		<?php 
			if($pagination == 1){
				paradise_fnc_pagination_nav( $number, $query->found_posts, $query->max_num_pages );
			}
		?>
	</div>
</div>
<?php else : ?>
<div><?php esc_html_e( 'No found record', 'paradise' ); ?></div>
<?php endif; ?>	
<?php 
wp_reset_query();
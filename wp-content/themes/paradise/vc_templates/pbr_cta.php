<?php
    $atts = vc_map_get_attributes( $this->getShortcode(), $atts );
    extract( $atts );

    $img = wp_get_attachment_image_src($photo,'full');
    $color = $color?'style="color:'. $color .';"' : "";
    $title_color = $title_color?'style="color:'. $title_color .';"' : "";
    if ( ! empty( $link ) ) {
        $link = vc_build_link( $link );
        $title = '<a ' .$title_color. ' href="' . esc_attr( $link['url'] ) . '"' . ( $link['target'] ? ' target="' . esc_attr( $link['target'] ) . '"' : '' ) . ( $link['rel'] ? ' rel="' . esc_attr( $link['rel'] ) . '"' : '' ) . ( $link['title'] ? ' title="' . esc_attr( $link['title'] ) . '"' : '' ) . '>' . $title . '</a>';
    }
?>
<div class="pbr-cta <?php echo esc_attr($el_class) ?> <?php echo esc_attr($title_align); ?>">
	<?php if(isset($img[0]) && $img[0]){?>
	<div class="pbr-image">
		<img src="<?php echo esc_url_raw($img[0]);?>" alt="<?php echo esc_attr($title); ?>" />
	</div>
	<?php } ?>
    
    <div class="pbr-content">
        <h4 <?php echo trim( $title_color); ?>> <?php if($icon){ ?><i class="icons <?php echo esc_attr($icon); ?>" <?php echo trim( $color); ?>></i> <?php } ?> <?php echo trim($title); ?></h4>
    </div>
</div>


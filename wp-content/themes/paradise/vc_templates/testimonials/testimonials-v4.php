<?php
    $subject = get_post_meta( get_the_ID(), 'paradise_testimonial_subject', true);
    $job = get_post_meta( get_the_ID(), 'paradise_testimonial_job', true);
?>
<div class="testimonials-body testimonials-v4 text-center">
   
   <?php if(!empty($subject) ): ?>
      <h4 class="subject"><?php echo trim($subject); ?></h4>
   <?php endif; ?>

   <div class="testimonials-quote clearfix"><?php the_content() ?></div>

   <div class="testimonials-profile"> 
     <div class="testimonials-avatar">
    	<div class="shadow-box">
    		<?php the_post_thumbnail('widget') ?>
    	</div>
         
      </div>
      <div class="testimonial-meta">
         <h6 class="name text-uppercase"> <?php the_title(); ?></h6>
         <?php if(!empty($job) ): ?>
            <div class="job hide"><?php echo trim($job); ?></div>
         <?php endif; ?>
         <div class="entry-date text-uppercase">
            <span><?php the_time( 'd' ); ?></span>
            <span><?php the_time( 'M' ); ?></span>,
            <span><?php the_time( 'Y' ); ?></span>
         </div>
      </div> 
   </div> 
   
</div>
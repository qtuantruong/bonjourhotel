
<?php
    $subject = get_post_meta( get_the_ID(), 'paradise_testimonial_subject', true);
    $job = get_post_meta( get_the_ID(), 'paradise_testimonial_job', true);
?>
<div class="testimonials-body">
	<div class="row">
		
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
			<div class="testimonials-bio text-right">
				<div class="testimonials-avatar">
					<div class="shadow-box"><a href="#"><?php the_post_thumbnail('widget', '', 'class="radius-x"');?></a></div>
				</div>
				<h5 class="testimonials-name">
			        <?php the_title(); ?>
			    </h5>  
			    <div class="entry-date text-uppercase">
		            <span><?php the_time( 'd' ); ?></span>
		            <span><?php the_time( 'M' ); ?></span>,
		            <span><?php the_time( 'Y' ); ?></span>
		         </div>
			</div>
		</div>

		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
			<?php if(!empty($subject) ): ?>
		    	<h4 class="subject"><?php echo trim($subject); ?></h4>
		    <?php endif; ?>
			<div class="testimonials-description"><?php the_content() ?></div>
		</div>

	</div>
    
</div>

<?php
    $subject = get_post_meta( get_the_ID(), 'paradise_testimonial_subject', true);
    $job = get_post_meta( get_the_ID(), 'paradise_testimonial_job', true);
?>

<div class="testimonials-body-wrapper">
  <div class="testimonials-body text-center">

    <div class="testimonials-avatar">
      <?php the_post_thumbnail('widget') ?>
    </div>
     
     <?php if(!empty($subject) ): ?>
        <h4 class="subject"><?php echo trim($subject); ?></h4>
     <?php endif; ?>

     <div class="testimonials-description"><?php the_content() ?></div>

     <div class="testimonials-profile"> 
       
        <h6 class="name text-uppercase"> <?php the_title(); ?></h6>
         <?php if(!empty($job) ): ?>
            <div class="job hide"><?php echo trim($job); ?></div>
         <?php endif; ?>
         <div class="entry-date text-uppercase">
            <span><?php the_time( 'd' ); ?></span>
            <span><?php the_time( 'M' ); ?></span>,
            <span><?php the_time( 'Y' ); ?></span>
         </div>

     </div> 
     
  </div>
</div>

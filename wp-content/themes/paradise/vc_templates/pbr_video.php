<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WPOpal  Team <opalwordpress@gmail.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
wp_enqueue_script( 'prettyphoto' );
wp_enqueue_style( 'prettyphoto' );

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$_id = paradise_fnc_makeid();
$_count = 0;

$args = array(
	'post_type' => 'video',
	'posts_per_page' => $number,
	'post_status' => 'publish',
);

if(is_front_page()){
    $paged = (get_query_var('page')) ? get_query_var('page') : 1;
}
else{
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
}
$args['paged'] = $paged; 
$query = new WP_Query($args);
?>

<div class="widget pbr-video <?php echo esc_attr($el_class); ?>">
	<?php if($query->have_posts()){ ?>
		<?php if($title!=''){ ?>
			<h3 class="widget-title">
				<span><?php echo trim($title); ?></span>
			</h3>
		<?php } ?>
 		<div class="widget-content">
			<?php $_count=0; while($query->have_posts()):$query->the_post(); $_count++; ?>
			<?php 
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );
				$url = get_post_meta( get_the_ID(), 'paradise_video_url', true);
				$url = empty($url)? '#' : $url;
			?>	
				<?php if($_count % $columns == 1) echo '<div class="row">'; ?>
					<div class="item col-sm-<?php echo floor(12/$columns) ?> col-xs-12">
						<div class="image">
							<?php the_post_thumbnail('thumbnail'); ?>
							<div class="mask"><a class="prettyphoto radius-x" href="<?php echo esc_url_raw($url); ?>"><i class="fa fa-play"></i></a></div>
						</div>
					</div>
				<?php if($_count % $columns == 0 || $_count == $query->found_posts) echo '</div>'; ?>

			<?php endwhile; ?>
		</div>

		<?php 
			if($pagination == 1){
				paradise_fnc_pagination_nav( $number, $query->found_posts, $query->max_num_pages );
			}
		?>

	<?php } ?>

</div>
<?php wp_reset_postdata(); ?>

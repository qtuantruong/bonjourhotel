<?php

$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

?>
<div class="widget pbr-pricing-widget <?php echo esc_attr($el_class); ?>">
    
    <div class="widget-content">

        <ul class="pricing-list">

            <?php
                $line_data = array();
                $items = (array) vc_param_group_parse_atts( $items );
                foreach ( $items as $data ) {
                    $new_line = $data;
                    $new_line['title'] = isset( $data['title'] ) ? $data['title'] : '';
                    $new_line['subtitle'] = isset( $data['subtitle'] ) ? $data['subtitle'] : '';
                    $new_line['price'] = isset( $data['price'] ) ? $data['price'] : '';
                    $new_line['currency'] = isset( $data['currency'] ) ? $data['currency'] : '';
                    $line_data[] = $new_line;
                }
            ?>

            <?php
                if($line_data): 
                $i = 1;
                foreach ($line_data as $key => $item):
            ?>
                <li class="pricing-plan clearfix">
                    <div class="pull-left">
                        <h5 class="plan-title"><?php echo esc_html($item['title']) ?></h5>
                        <span class="plan-subtitle"><?php echo esc_html($item['subtitle']) ?></span>
                    </div>
                    <div class="pull-right">
                        <sup class="plan-currency"><?php echo esc_html($item['currency']); ?></sup>
                        <span class="plan-price"><?php echo esc_html($item['price']); ?></span>
                    </div> 
                  </li>
            <?php $i++; endforeach; endif; ?>  
        </ul>

    </div>
    
</div>
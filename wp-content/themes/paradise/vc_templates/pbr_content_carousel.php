<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$items = (array) vc_param_group_parse_atts( $items );

if ( !empty($items) ):
	$_total = count($items);
	$columns_count = 2;
?>
	<div class="widget widget-content-carousel widget-content style-lighten <?php echo ($el_class!='')?' '.esc_attr( $el_class ):''; ?>">
			
			<?php if($title!=''):?>
				<div class="tabs-container tab-heading text-center clearfix">
					<h3 class="widget-title">
		        		<span><span><?php echo esc_attr( $title ); ?></span></span>
		        		<?php if ($subtitle != '') {?>
		        			<span class="subtitle"><?php echo esc_attr( $subtitle ); ?></span>
		        		<?php } ?>
					</h3>
				</div>
			<?php endif; ?>
			<div class="carousel-wrapper">
				<div class="owl-carousel-play" data-ride="owlcarousel">
			      <div class="owl-carousel products" data-slide="<?php echo esc_attr($columns_count); ?>" data-pagination="true" data-navigation="true">
						<?php foreach ($items as $item): ?>
							<div class="item">
								<div class="item-inner">
			                    <div class="item-top">
				                     <?php if ( isset($item['title']) && !empty($item['title']) ): ?>
					                     
					                    	<span class="title"><?php echo esc_html($item['title']); ?></span>
					                  <?php endif; ?>
				                  </div>
			                    <?php if ( isset($item['description']) && !empty($item['description']) ): ?>
			                    	<div class="description">
			                    			<?php echo trim( $item['description'] ); ?>
			                    	</div>
			                    <?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
	</div>
<?php endif; ?>
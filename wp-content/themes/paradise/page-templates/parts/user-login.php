<ul class="list-inline">
	
	    <?php if( !is_user_logged_in() ){ ?>
	    <!-- Custom link register-->
		    <?php if(paradise_fnc_theme_options('enable-register', true)): ?>
			    	<li>
			    		<a class="btn btn-primary btn-3d radius-6x" href="<?php echo esc_url(paradise_fnc_theme_options('link-register', '#')); ?>" title="<?php esc_html_e('Sign up','paradise'); ?>"> <?php esc_html_e('Sign up', 'paradise'); ?> </a>
			    	</li>
	    	<?php else: ?>
	    			<li>
	    				<a data-toggle="modal" data-target="#modalRegisterForm" class="btn btn-primary btn-3d radius-6x" href="#" title="<?php esc_html_e('Sign up','paradise'); ?>"> <?php esc_html_e('Sign up', 'paradise'); ?>
	    				</a>
    				</li>
    		<?php endif; ?>
		<!-- End link register-->

		<!-- Custom link login-->
		<?php if(paradise_fnc_theme_options('enable-login', true)): ?>
	        	<li>
	        		<a href="<?php echo esc_url(paradise_fnc_theme_options('link-login', '#')); ?>" class="pbr-user-login btn btn-white btn-3d radius-6x"><?php esc_html_e( 'Sign In','paradise' ); ?></a>
        		</li>
	      <?php else: ?>
	        	<li>
	        		<a href="#"  data-toggle="modal" data-target="#modalLoginForm" class="pbr-user-login btn btn-white btn-3d radius-6x"><?php esc_html_e( 'Sign In','paradise' ); ?>
	        		</a>
    			</li>
			<?php endif; ?>
		<!-- End link login-->
	    <?php }else{ ?>
	        <?php $current_user = wp_get_current_user(); ?>
	      <li>  <span class="hidden-xs"><?php echo esc_html__('Welcome ','paradise'); ?><?php echo esc_html( $current_user->display_name); ?> !</span></li>
	    <?php } ?>
</ul>
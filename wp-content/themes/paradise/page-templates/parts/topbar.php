<section id="pbr-topbar" class="pbr-topbar">
	<div class="container clearfix">

        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-9 hidden-xs">
                 <div class="left-top-bar">
                    <?php dynamic_sidebar( 'topbar-left' ); ?>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                <!-- <div class="right-top-bar pull-right">
                    <?php //dynamic_sidebar( 'topbar-right' ); ?>
                </div> -->
                <?php if( paradise_fnc_theme_options('showuserbox') ) : ?>
                    <div class="user-login pull-right">
                        <ul class="list-inline">
                            
                            <?php do_action('opal-account-buttons'); ?> 
                        
                        </ul>                 
                    </div>
                <?php endif; ?>
            </div>
            
        </div>
        <!-- .row -->
                
        <div class="pull-left hidden-xs hidden-sm">
            
            <?php 
                 // WPML - Custom Languages Menu
            	paradise_fnc_wpml_language_buttons();
            ?>
            <?php if(has_nav_menu( 'topmenu' )): ?>
 
            <nav class="pbr-topmenu" role="navigation">
                <?php
                    $args = array(
                        'theme_location'  => 'topmenu',
                        'menu_class'      => 'pbr-menu-top list-inline list-square',
                        'fallback_cb'     => '',
                        'menu_id'         => 'main-topmenu'
                    );
                    wp_nav_menu($args);
                ?>
            </nav>
   
            <?php endif; ?>                            
        </div>
            

	</div>	
</section>
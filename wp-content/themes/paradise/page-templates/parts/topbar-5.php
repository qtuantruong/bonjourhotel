<section id="pbr-topbar" class="pbr-topbar pbr-topbar-5 hidden-xs">
	<div class="container">
        <div class="clearfix">
            <div class="pull-right">
                <div class="left-top-bar pull-left">
                    <?php dynamic_sidebar( 'topbar-left' ); ?>
                </div>
                <?php if( paradise_fnc_theme_options('showuserbox') ) : ?>
                    <div class="user-login pull-left">
                        <ul class="list-inline">
                            
                            <?php do_action('opal-account-buttons'); ?> 
                        
                        </ul>                 
                    </div>
                <?php endif; ?>
                <div class="pull-left hidden-xs hidden-sm">
            
                    <?php 
                         // WPML - Custom Languages Menu
                        paradise_fnc_wpml_language_buttons();
                    ?>
                    <?php if(has_nav_menu( 'topmenu' )): ?>
         
                    <nav class="pbr-topmenu" role="navigation">
                        <?php
                            $args = array(
                                'theme_location'  => 'topmenu',
                                'menu_class'      => 'pbr-menu-top list-inline list-square',
                                'fallback_cb'     => '',
                                'menu_id'         => 'main-topmenu'
                            );
                            wp_nav_menu($args);
                        ?>
                    </nav>
           
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
    </div>
    <!-- .row -->
</section>
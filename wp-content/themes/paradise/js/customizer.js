/**
 * PrestaBase Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Customizer preview reload changes asynchronously.
 */
( function( $ ) {
	// Site title and description.
	wp.customize( 'blogname', function( value ) {
		value.bind( function( to ) {
			$( '.site-title a' ).text( to );
		} );
	} );
	wp.customize( 'blogdescription', function( value ) {
		value.bind( function( to ) {
			$( '.site-description' ).text( to );
		} );
	} );
	// Header text color.
	wp.customize( 'header_textcolor', function( value ) {
		value.bind( function( to ) {
			if ( 'blank' === to ) {
				$( '.site-title, .site-description' ).css( {
					'clip': 'rect(1px, 1px, 1px, 1px)',
					'position': 'absolute'
				} );
			} else {
				$( '.site-title,  .site-description' ).css( {
					'clip': 'auto',
					'position': 'static'
				} );

				$( '.site-title a' ).css( {
					'color': to
				} );
			}
		} );
	} );
 

	//Update site link color in real time...
	wp.customize( 'theme_color', function( value ) {
			
		var selectors = "#pbr-topbar i, .navbar-mega .navbar-nav li.active > a , .navbar-mega .navbar-nav li > a:hover, .navbar-mega .navbar-nav li.active > a .caret, .widget-text-heading .description, .widget-text-heading .widget-desc, .text-primary,";
		selectors += ".widget-text-heading .widget-heading em, .opalhotel-price .price-value, .reservation-link, .sp-selected .title,  .opalhotel-room-meta li:before,.opalhotel-room-price ins";
	 
		value.bind( function( newval ) {  
			$( selectors ).css('color', newval );
		} );
	} );

	wp.customize( 'theme_color', function( value ) {
		 	
		var selectors = ".button.button-theme, .btn-primary, .scrollup,.vc_tta-style-flat .vc_tta-tabs-list .vc_tta-tab.vc_active, .entry-meta .entry-date, .owl-controls .owl-page.active span,";
		selectors += ".special-image:before, .special-image:after, .button-primary-inverse, .opalhotel_checkout_booking_detail,.opalhotel-reservation-process-steps ul li.active span,.opalhotel-reservation-process-steps ul li:after,";
		selectors += " .vc_tta-tab > a:after,.vc_toggle.vc_toggle_active .vc_toggle_title, a.tribe-events-gcal, a.tribe-events-ical, .post-sub-content .entry-date";
	 
		value.bind( function( newval ) {  
			$( selectors ).css('background-color', newval );
			$( selectors ).css('border-color', newval );
		} );

	} );



	//Update site link color in real time...
	wp.customize( 'page_bg', function( value ) {
		value.bind( function( newval ) {  
			$('#page').css('background-color', newval );
		} );
	} );


	//Update site link color in real time...
	wp.customize( 'body_text_color', function( value ) {
		value.bind( function( newval ) {  
			$('body').css('color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'topnav_bg', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-masthead.header-absolute, #pbr-masthead').css('background-color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'topnav_color', function( value ) {
		value.bind( function( newval ) {  
			$('.navbar-mega .navbar-nav li > a, .navbar-mega .navbar-nav li.active > a').css('color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'topbar_bg', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-topbar').css('background-color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'topbar_color', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-topbar, #pbr-topbar a, #pbr-topbar span').css('color', newval );
		} );
	} );


	//Update site link color in real time...
	wp.customize( 'topnav_bg', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-masthead.header-absolute, #pbr-masthead').css('background-color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'topnav_color', function( value ) {
		value.bind( function( newval ) {  
			$('.navbar-mega .navbar-nav li > a, .navbar-mega .navbar-nav li.active > a').css('color', newval );
		} );
	} );




	//Update site link color in real time...
	wp.customize( 'footer_bg', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-footer').css('background-color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'footer_color', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-footer, #pbr-footer a').css('color', newval );
		} );
	} );

	//Update site link color in real time...
	wp.customize( 'footer_heading_color', function( value ) {
		value.bind( function( newval ) {  
			$('#pbr-footer h2, #pbr-footer h3, #pbr-footer h4').css('color', newval );
		} );
	} );
} )( jQuery );
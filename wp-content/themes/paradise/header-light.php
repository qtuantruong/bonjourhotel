<?php
/**
 * The Header for our theme: Top has Logo left + search right . Below is horizal main menu
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site"><div class="pbr-page-inner row-offcanvas row-offcanvas-left mobile-header-light">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header" class="hidden-xs hidden-sm">
		<a href="<?php echo esc_url( get_option('header_image_link','#') ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>
	<?php do_action( 'paradise_template_header_before' ); ?>
	<header id="pbr-masthead" class="site-header pbr-header-light <?php if( paradise_fnc_theme_options('keepheader') ) : ?> keep-header<?php endif; ?>"" role="banner">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="header-main text-center clearfix">

						<section id="pbr-mainmenu" class="pbr-mainmenu clearfix">

							<div class="inner navbar-mega-simple"><?php get_template_part( 'page-templates/parts/nav' ); ?></div>

						</section>
						<!-- #pbr-mainmenu -->

					</div>
				</div>
				<!-- .col-md-12 -->
			</div>
		</div>	
	</header><!-- #masthead -->

	<?php do_action( 'paradise_template_header_after' ); ?>
	
	<section id="main" class="site-main">

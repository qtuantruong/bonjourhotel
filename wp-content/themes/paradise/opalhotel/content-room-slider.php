<?php 
	global $page_link;
	global $opalhotel_room;
?>
<div <?php post_class( ); ?>>
	<div class="room-grid">
		<div class="room-top-wrap">
			<?php
				/**
				 * opalhotel_archive_loop_item_thumbnail hook.
				 * opalhotel_loop_item_thumbnail - 5
				 */
				do_action( 'opalhotel_archive_loop_item_thumbnail' );

			?>
            <div class="room-content">
                <?php
                /**
                 * opalhotel_before_archive_loop_item_title hook.
                 * opalhotel_loop_item_title - 5
                 */
                do_action( 'opalhotel_archive_loop_item_title' );
                ?>
                <?php
                /**
                 * opalhotel_before_archive_loop_item_title hook.
                 * opalhotel_loop_item_title - 5
                 */
                do_action( 'opalhotel_archive_loop_item_price' );
                ?>
            </div>
        </div>

		<?php
			/**
			 * opalhotel_after_archive_loop_item hook.
			 */
			do_action( 'opalhotel_after_archive_loop_item' );
		?>
	</div>
</div>
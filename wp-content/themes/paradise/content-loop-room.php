<?php 
	global $page_link;
	global $opalhotel_room;
?>
<div <?php post_class( ); ?>>
	<div class="room-grid">
		<?php
			/**
			 * opalhotel_before_archive_loop_item_title hook.
			 * opalhotel_loop_item_title - 5
			 */
			do_action( 'opalhotel_archive_loop_item_title' );
		?>
		<div class="room-top-wrap">
			<?php if($opalhotel_room->has_discounts() ) : ?>
				<span class="room-label room-label-discount"><?php _e( 'Discount', 'paradise'); ?></span>
			<?php endif; ?>
			<?php
				/**
				 * opalhotel_archive_loop_item_thumbnail hook.
				 * opalhotel_loop_item_thumbnail - 5
				 */
				do_action( 'opalhotel_archive_loop_item_thumbnail' );

			?>
		</div>
		<footer>
			<?php
				/**
				 * opalhotel_before_archive_loop_item_title hook.
				 * opalhotel_loop_item_title - 5
				 */
				do_action( 'opalhotel_archive_loop_item_price' );
			?>
			<?php

				/**
				 * opalhotel_archive_loop_item_title hook.
				 *
				 * @hooked opalhotel_loop_item_description - 5
				 */
				do_action( 'opalhotel_archive_loop_item_description' );
			?>
		</footer>

		<?php
			/**
			 * opalhotel_after_archive_loop_item hook.
			 */
			do_action( 'opalhotel_after_archive_loop_item' );
		?>
	</div>
</div>
<?php
/**
 * paradise functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link https://codex.wordpress.org/Plugin_API
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
define( 'PARADISE_THEME_VERSION', '1.0' );
 
/**
 * Set up the content width value based on the theme's design.
 *
 * @see paradise_fnc_content_width()
 *
 * @since paradise 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

 
if ( ! function_exists( 'paradise_fnc_setup' ) ) :
/**
 * mode setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since paradise 1.0
 */
function paradise_fnc_setup() {

	/*
	 * Make mode available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on mode, use a find and
	 * replace to change 'paradise' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'paradise', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
 	add_editor_style(  );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'post-fullwidth', 1038, 9999, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => esc_html__( 'Top primary menu', 'paradise' ),
		'secondary' => esc_html__( 'Secondary menu in left sidebar', 'paradise' ),
		'topmenu'	=> esc_html__( 'Topbar Menu in Topbar sidebar', 'paradise' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'paradise_fnc_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

	
	// add support for display browser title
	add_theme_support( 'title-tag' );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
	
	paradise_fnc_get_load_plugins();

}
endif; // paradise_fnc_setup
add_action( 'after_setup_theme', 'paradise_fnc_setup' );

/**
 * batch including all files in a path.
 *
 * @param String $path : PATH_DIR/*.php or PATH_DIR with $ifiles not empty
 */
function paradise_pbr_includes( $path, $ifiles=array() ){

    if( !empty($ifiles) ){
         foreach( $ifiles as $key => $file ){
            $file  = $path.'/'.$file; 
            if(is_file($file)){
                require($file);
            }
         }   
    }else {
        $files = glob($path);
        foreach ($files as $key => $file) {
            if(is_file($file)){
                require($file);
            }
        }
    }
}

/**
 * Get Theme Option Value.
 * @param String $name : name of prameters 
 */
function paradise_fnc_theme_options($name, $default = false) {
  
    // get the meta from the database
    $options = ( get_option( 'pbr_theme_options' ) ) ? get_option( 'pbr_theme_options' ) : null;

    
   
    // return the option if it exists
    if ( isset( $options[$name] ) ) {
        return apply_filters( 'pbr_theme_options_$name', $options[ $name ] );
    }
    if( get_option( $name ) ){
        return get_option( $name );
    }
    // return default if nothing else
    return apply_filters( 'pbr_theme_options_$name', $default );
}
/**
 * Adjust content_width value for image attachment template.
 *
 * @since paradise 1.0
 */
function paradise_fnc_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'paradise_fnc_content_width' );


/**
 * Require function for including 3rd plugins
 *
 */
paradise_pbr_includes(  get_template_directory() . '/inc/plugins/*.php' );

function paradise_fnc_get_load_plugins(){

	$plugins[] =(array(
		'name'                     => esc_html__('MetaBox','paradise'), // The plugin name
	    'slug'                     => 'meta-box', // The plugin slug (typically the folder name)
	    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Contact Form 7','paradise'), // The plugin name
	    'slug'                     => 'contact-form-7', // The plugin slug (typically the folder name)
	    'required'                 => true, // If false, the plugin is only 'recommended' instead of required
	));

	$plugins[] =(array(
		'name'                     => esc_html__('MailChimp', 'paradise'),// The plugin name
	    'slug'                     => 'mailchimp-for-wp', // The plugin slug (typically the folder name)
	    'required'                 =>  true
	));

	$plugins[] =(array(
		'name'                     => esc_html__('WPBakery Visual Composer', 'paradise'), // The plugin name
	    'slug'                     => 'js_composer', // The plugin slug (typically the folder name)
	    'required'                 => true,
	    'source'				   => esc_url('http://www.wpopal.com/thememods/js_composer.zip') 
	));

	$plugins[] =(array(
       'name'                      => esc_html__('The Events Calendar', 'paradise' ), // The plugin name
       'slug'                     => 'the-events-calendar', // The plugin slug (typically the folder name)
       'required'                 => true, // If false, the plugin is only 'recommended' instead of required
    ));

	$plugins[] =(array(
		'name'                     => esc_html__('Revolution Slider', 'paradise'), // The plugin name
        'slug'                     => 'revslider', // The plugin slug (typically the folder name)
        'required'                 => true ,
        'source'				   => esc_url('http://www.wpopal.com/thememods/revslider.zip')
	));

	$plugins[] =(array(
		'name'                     => esc_html__('PBR Themer For Themes', 'paradise'), // The plugin name
        'slug'                     => 'pbrthemer', // The plugin slug (typically the folder name)
        'required'                 => true ,
        'source'				   => esc_url('http://www.wpopal.com/themeframework/pbrthemer.zip')
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Opal Hotel Room Booking','paradise'), // The plugin name
        'slug'                     => "opal-hotel-room-booking", // The plugin slug (typically the folder name)
        'required'                 => true 
	));

	$plugins[] =(array(
		'name'                     => esc_html__('Instagram','paradise'), // The plugin name
        'slug'                     => "wd-instagram-feed", // The plugin slug (typically the folder name)
        'required'                 => true 
	));

	tgmpa( $plugins );
}

/**
 * Register three mode widget areas.
 *
 * @since paradise 1.0
 */
function paradise_fnc_registart_widgets_sidebars() {
	 
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Sidebar Default', 'paradise' ),
		'id'            => 'sidebar-default',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Topbar Left' , 'paradise'),
		'id'            => 'topbar-left',
		'description'   => esc_html__( 'Appears in the topbar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span</span></h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Topbar Right' , 'paradise'),
		'id'            => 'topbar-right',
		'description'   => esc_html__( 'Appears in the topbar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Newsletter' , 'paradise'),
		'id'            => 'newsletter',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Twitter' , 'paradise'),
		'id'            => 'twitter',
		'description'   => esc_html__( 'Appears in the footer.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Left Sidebar' , 'paradise'),
		'id'            => 'sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style  clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));
	register_sidebar(
	array(
		'name'          => esc_html__( 'Right Sidebar' , 'paradise'),
		'id'            => 'sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'Blog Left Sidebar' , 'paradise'),
		'id'            => 'blog-sidebar-left',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'Blog Right Sidebar', 'paradise'),
		'id'            => 'blog-sidebar-right',
		'description'   => esc_html__( 'Appears on posts and pages in the sidebar.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget widget-style clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span><span>',
		'after_title'   => '</span></span></h3>',
	));


	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 1' , 'paradise'),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 2' , 'paradise'),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 3' , 'paradise'),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Footer 4' , 'paradise'),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Appears in the footer section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'Instagram' , 'paradise'),
		'id'            => 'instagram',
		'description'   => esc_html__( 'Appears in the bottom section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
	register_sidebar( 
	array(
		'name'          => esc_html__( 'CopyRight Full' , 'paradise'),
		'id'            => 'copyright',
		'description'   => esc_html__( 'Appears in the bottom section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));

	register_sidebar( 
	array(
		'name'          => esc_html__( 'CopyRight Socials' , 'paradise'),
		'id'            => 'copyright-social',
		'description'   => esc_html__( 'Appears in the copyright section of the site.', 'paradise'),
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title"><span>',
		'after_title'   => '</span></h3>',
	));
}
add_action( 'widgets_init', 'paradise_fnc_registart_widgets_sidebars' );

/**
 * Register Lato Google font for mode.
 *
 * @since paradise 1.0
 *
 * @return string
 */
function paradise_fnc_font_url() {
	 
	$fonts_url = '';
 
    /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lora = _x( 'on', 'Hind font: on or off', 'paradise' );
 
    /* Translators: If there are characters in your language that are not
    * supported by Lato, translate this to 'off'. Do not translate
    * into your own language.
    */
    $lato = _x( 'on', 'Lato font: on or off', 'paradise' );

    $abhaya = _x( 'on', 'Abhaya Libre font: on or off', 'paradise' );

    $hind = _x( 'on', 'Hind font: on or off', 'paradise' );

    $poppins = _x( 'on', 'Poppins font: on or off', 'paradise' );

    $pridi = _x( 'on', 'Pridi font: on or off', 'paradise' );

    $teko = _x( 'on', 'Teko font: on or off', 'paradise' );

    $sourcesanspro = _x( 'on', 'Source Sans Pro font: on or off', 'paradise' );
 
    if ( 'off' !== $lora || 'off' !== $lato ) {
        $font_families = array();
 
        if ( 'off' !== $lora ) {
            $font_families[] = 'Playfair+Display:400,400italic,700,700italic,900,900italic';
        }
 
        if ( 'off' !== $lato ) {
            $font_families[] = 'Lato:400,800,700italic,700,400italic,300,300italic,900';
        }

        if ( 'off' !== $abhaya ) {
            $font_families[] = 'Abhaya+Libre:400,500,600,700,800';
        }

        if ( 'off' !== $hind ) {
            $font_families[] = 'Hind:300,400,500,600,700';
        }

        if ( 'off' !== $poppins ) {
            $font_families[] = 'Poppins:300,400,500,600,700';
        }

        if ( 'off' !== $pridi ) {
            $font_families[] = 'Pridi:200,300,400,500,600,700';
        }

        if ( 'off' !== $teko ) {
            $font_families[] = 'Teko:300,400,500,600,700';
        }

        if ( 'off' !== $sourcesanspro ) {
            $font_families[] = 'Source+Sans+Pro:300,300i,400,600,700,900';
        }
 
        $query_args = array(
            'family' => ( implode( '%7C', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
        );
 		
 		 
 		$protocol = is_ssl() ? 'https:' : 'http:';
        $fonts_url = add_query_arg( $query_args, $protocol .'//fonts.googleapis.com/css' );
    }
 
    return esc_url_raw( $fonts_url );
}

function paradise_add_sourcesanspro_font($fonts_list){
    $sourcesanspro->font_family = 'Source Sans Pro';
    $sourcesanspro->font_types = "300 light:300:normal, 300 light italic:300:italic,400 regular:400:normal, 600 semi-bold:600:normal, 700 bold:700:normal, 900 black:900:normal";
    $sourcesanspro->font_styles = 'light';
    $sourcesanspro->font_family_description = 'Choose your font name you want';
    $sourcesanspro->font_style_description = 'Choose your font style you want';
    $fonts_list[] = $sourcesanspro;

    return $fonts_list;
}
add_filter('vc_google_fonts_get_fonts_filter', 'paradise_add_sourcesanspro_font');

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since paradise 1.0
 */
function paradise_fnc_scripts() {
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'paradise-lato', paradise_fnc_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '3.0.3' );

    wp_dequeue_style( 'opal-hotel-room-booking' );
	wp_enqueue_style( 'opalhotel', get_template_directory_uri().'/css/opalhotel.css', array() );

	if(isset($_GET['pbr-skin']) && $_GET['pbr-skin']) {
		$currentSkin = $_GET['pbr-skin'];
	}else{
		$currentSkin = str_replace( '.css','', paradise_fnc_theme_options('skin','default') );
	}
	if( is_rtl() ){
		if( !empty($currentSkin) && $currentSkin != 'default' ){ 
			wp_enqueue_style( 'paradise-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/rtl-'.$currentSkin.'/style.css' );
		}else {
			// Load our main stylesheet.
			wp_enqueue_style( 'paradise-style', get_template_directory_uri() . '/css/rtl-style.css' );
		}
	}
	else {
		if( !empty($currentSkin) && $currentSkin != 'default' ){ 
			wp_enqueue_style( 'paradise-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/'.$currentSkin.'/style.css' );
			wp_enqueue_style( 'opalhotel-'.$currentSkin.'-style', get_template_directory_uri() . '/css/skins/'.$currentSkin.'/opalhotel.css' );
		}else {
			// Load our main stylesheet.
			wp_enqueue_style( 'paradise-style', get_template_directory_uri() . '/css/style.css' );
			wp_enqueue_style( 'opalhotel'		   , get_template_directory_uri() . '/css/opalhotel.css');
		}	
	}	
	

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'paradise-ie', get_template_directory_uri() . '/css/ie.css', array( 'paradise-style' ), '20131205' );
	wp_style_add_data( 'paradise-ie', 'conditional', 'lt IE 9' );


	
	wp_enqueue_script( 'paradise-bootstrap-min', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '20130402' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}


	wp_enqueue_script( 'paradise-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );

	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl-carousel/owl.carousel.js', array( 'jquery' ), '20150315', true );

	wp_enqueue_script( 'waypoint-script', get_template_directory_uri() . '/js/waypoints.min.js', array( 'jquery' ), '20150315', true );

    wp_enqueue_script( 'stickysidebar-js'    ,	get_template_directory_uri().'/js/sticky-kit.js', array( 'jquery' ), '20150315', true );

	wp_localize_script( 'paradise-script', 'paradiseAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

	wp_enqueue_script('prettyphoto-js', get_template_directory_uri().'/js/jquery.prettyPhoto.js');
	wp_enqueue_style('prettyPhoto', get_template_directory_uri().'/css/prettyPhoto.css' );

}
add_action( 'wp_enqueue_scripts', 'paradise_fnc_scripts', 12 );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since paradise 1.0
 */
function paradise_fnc_admin_fonts() {
	wp_enqueue_style( 'paradise-lato', paradise_fnc_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'paradise_fnc_admin_fonts' );



/**
 * Implement rick meta box for post and page, custom post types. These 're used with metabox plugins
 */
if( is_admin() ){
	require_once(  get_template_directory() . '/inc/admin/metabox/pagepost.php' );
	require_once(  get_template_directory() . '/inc/admin/metabox/testimonial.php' );
	require_once(  get_template_directory() . '/inc/admin/metabox/video.php' );
	require_once(  get_template_directory() . '/inc/admin/function.php' );
}

require_once(  get_template_directory() . '/inc/classes/nav.php' );
require_once(  get_template_directory() . '/inc/classes/offcanvas-menu.php' );

require_once(  get_template_directory() . '/inc/custom-header.php' );
require_once(  get_template_directory() . '/inc/customizer.php' );
require_once(  get_template_directory() . '/inc/function-post.php' );
require_once(  get_template_directory() . '/inc/function-unilty.php' );
require_once(  get_template_directory() . '/inc/functions-import.php' );
require_once(  get_template_directory() . '/inc/template-hook.php' );
require_once(  get_template_directory() . '/inc/template-tags.php' );
require_once(  get_template_directory() . '/inc/template.php' );
require_once(  get_template_directory() . '/inc/vendors/widgets/copyright.php' );
 
/**
 * Check and load to support visual composer
 */
if(  in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && class_exists('WPBakeryVisualComposerAbstract') ){
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-elements.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-extends.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-news.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/class-vc-theme.php' );
	require_once(  get_template_directory() . '/inc/vendors/visualcomposer/function-vc.php' );

}

/**
 * Check and load to support visual composer
 */
if(  in_array( 'opal-hotel-room-booking/opal-hotel-room-booking.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	require_once(  get_template_directory() . '/inc/vendors/opalhotel/functions-hook.php' );
	require_once(  get_template_directory() . '/inc/vendors/opalhotel/vc.php' );
}

/**
 * Check and load to support visual composer
 */
if( in_array( 'the-events-calendar/the-events-calendar.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	require_once(  get_template_directory() . '/inc/vendors/event/class-vc-event.php' );
	require_once(  get_template_directory() . '/inc/vendors/event/event-settings.php' );
	require_once(  get_template_directory() . '/inc/vendors/event/function-logic.php' );
}

if ( class_exists('PBR_User_Account') ) {
	$PBR_User_Account = new PBR_User_Account();
}
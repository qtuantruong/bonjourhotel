<?php
/**
 * The template for displaying Category pages
 *
 * @link http://wpopal.com/themes/mode
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */


get_header( apply_filters( 'paradise_fnc_get_header_layout', null ) ); ?>
<?php do_action( 'paradise_template_main_before' ); ?>
<section id="main-container" class="<?php echo apply_filters('paradise_template_main_container_class','container');?> inner <?php echo paradise_fnc_theme_options('blog-archive-layout') ; ?>">
	<div class="row">


		<div id="main-content" class="main-content col-lg-12">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					<?php if ( have_posts() ) : ?>

					<header class="archive-header">
						<h1 class="archive-title">
							<?php
								/*
								 * Queue the first post, that way we know what author
								 * we're dealing with (if that is the case).
								 *
								 * We reset this later so we can run the loop properly
								 * with a call to rewind_posts().
								 */
								the_post();

								printf( esc_html__( 'All posts by %s', 'paradise' ), get_the_author() );
							?>
						</h1>
						<?php if ( get_the_author_meta( 'description' ) ) : ?>
						<div class="author-description"><?php the_author_meta( 'description' ); ?></div>
						<?php endif; ?>
					</header><!-- .archive-header -->

					<?php
							/*
							 * Since we called the_post() above, we need to rewind
							 * the loop back to the beginning that way we can run
							 * the loop properly, in full.
							 */
							rewind_posts();

							/**
							 * 1-column or n-columns layout
							 */
							get_template_part( 'content', 'gridposts' );

							// Previous/next page navigation.
							paradise_fnc_paging_nav();

						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );

						endif;
					?>

				</div><!-- #content 
			</div><!-- #primary -->
			<?php get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->
 
	 
	 
	</div>	
</section>
<?php
get_footer();
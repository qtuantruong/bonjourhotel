<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
$footer_profile =  apply_filters( 'paradise_fnc_get_footer_profile', 'default' );
$post = get_post( $footer_profile );
$class = "";
if(is_object($post) && !empty($post)){
	$class = $post->post_name;
}
?>

		</section><!-- #main -->
		<?php do_action( 'paradise_template_main_after' ); ?>
		<?php //do_action( 'paradise_template_footer_before' ); ?>
		<section class="footer-wrapper">
					
			<footer id="pbr-footer" class="pbr-footer pbr-<?php echo esc_attr( $class ); ?>" role="contentinfo">
				<?php if( $footer_profile ) : ?>
				<div class="inner">
					<div class="pbr-footer-profile <?php echo esc_attr( $class ); ?>">
						<?php paradise_fnc_render_post_content( $footer_profile ); ?>
					</div>
				</div>
				<?php else: ?>
				<div class="inner">
					<?php get_sidebar( 'footer' ); ?>
				</div>
				<?php endif; ?>
					
			</footer><!-- #colophon -->
			<?php if( !$footer_profile ) : ?>
			<section class="pbr-copyright">
				<?php get_template_part( 'page-templates/parts/footer-bg' ); ?>
				<div class="container">
					<a href="#" class="scrollup"><span class="fa fa-angle-up"></span></a>
					<div class="text-center"><?php //dynamic_sidebar('copyright-social'); ?></div>
					<div class="site-info">
						<?php do_action( 'paradise_fnc_credits' ); ?>
						<?php paradise_display_footer_copyright(); ?>
					</div><!-- .site-info -->
				</div>	
			</section>
			<?php endif; ?>

		</section>
		<!-- .footer-wrapper -->
		
	
		<?php do_action( 'paradise_template_footer_after' ); ?>
		<?php get_sidebar( 'offcanvas' );  ?>
	</div>
</div>
	<!-- #page -->
<?php //get_sidebar( 'popup-sidebar' );  ?>
<?php wp_footer(); ?>
</body>
</html>
<?php
/**
 * The template for displaying Category pages
 *
 * @link http://wpopal.com/themes/mode
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
global $paradise_page_layouts; 
$paradise_page_layouts = apply_filters( 'paradise_fnc_get_single_room_sidebar_configs', null ); // echo '<Pre>'.print_r($paradise_page_layouts,1 ); die; 

get_header( apply_filters( 'paradise_fnc_get_header_layout', null ) ); ?>

<?php do_action( 'paradise_template_main_before' ); ?>
<section id="main-container" class="<?php echo apply_filters('paradise_template_main_container_class','container');?> inner <?php echo paradise_fnc_theme_options('blog-archive-layout') ; ?>">
	<div class="row">

		<div id="main-content" class="main-content col-sm-12 <?php echo esc_attr($paradise_page_layouts['main']['class']); ?>">
			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">

					<?php if ( have_posts() ) :  

							while ( have_posts() ) : the_post();  

								opalhotel_get_template_part( 'content', 'single-room' ); 

		 					endwhile; 

							// Previous/next page navigation.
							// paradise_fnc_paging_nav();

						else :
							// If no content, include the "No posts found" template.
							get_template_part( 'content', 'none' );

						endif;
					?>
				</div><!-- #content -->
			</div><!-- #primary -->
			<?php get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->

        <?php if( isset($paradise_page_layouts['sidebars']) && !empty($paradise_page_layouts['sidebars']) ) : ?>
            <?php get_sidebar(); ?>
        <?php endif; ?>

	</div>
	<?php
		do_action( 'opalhotel_after_main_content' );
 
		
	?>
</section>
<?php
get_footer();

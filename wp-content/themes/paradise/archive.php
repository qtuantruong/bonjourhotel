<?php
/**
 * The template for displaying Category pages
 *
 * @link http://wpopal.com/themes/mode
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
global $paradise_page_layouts;  
$paradise_page_layouts = apply_filters( 'paradise_fnc_get_archive_sidebar_configs', null );

get_header( apply_filters( 'paradise_fnc_get_header_layout', null ) ); ?>
<?php do_action( 'paradise_template_main_before' ); ?>
<section id="main-container" class="<?php echo apply_filters('paradise_template_main_container_class','container');?> inner <?php echo paradise_fnc_theme_options('blog-archive-layout') ; ?>">
	<div class="row">

		<?php if( isset($paradise_page_layouts['sidebars']) && !empty($paradise_page_layouts['sidebars']) ) : ?>
			<?php get_sidebar(); ?>
		<?php endif; ?>
		
		<div id="main-content" class="main-content  col-sm-12 <?php echo esc_attr($paradise_page_layouts['main']['class']); ?>">
			<div id="primary" class="content-area">
			 <div id="content" class="site-content" role="main">

					<?php if ( have_posts() ) : ?>

						<header class="page-header">
							<h1 class="page-title">
								<?php
									if ( is_day() ) :
										printf( esc_html__( 'Daily Archives: %s', 'paradise' ), get_the_date() );

									elseif ( is_month() ) :
										printf( esc_html__( 'Monthly Archives: %s', 'paradise' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'paradise' ) ) );

									elseif ( is_year() ) :
										printf( esc_html__( 'Yearly Archives: %s', 'paradise' ), get_the_date( _x( 'Y', 'yearly archives date format', 'paradise' ) ) );

									else :
										esc_html_e( 'Archives', 'paradise' );

									endif;
								?>
							</h1>
						</header><!-- .page-header -->

						<?php
								/**
								 * 1-column or n-columns layout
								 */
								get_template_part( 'content', 'gridposts' );

								// Previous/next page navigation.
								paradise_fnc_paging_nav();

							else :
								// If no content, include the "No posts found" template.
								get_template_part( 'content', 'none' );

							endif;
						?>
					</div><!-- #content -->

				
			</div><!-- #primary -->
			<?php get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->


 
	</div>	
</section>
<?php
get_footer();
 

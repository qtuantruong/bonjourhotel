<?php 
/**
 * Remove javascript and css files not use
 */


/**
 * Hook to top bar layout
 */
function paradise_fnc_topbar_layout(){
	$layout = paradise_fnc_get_header_layout();
	get_template_part( 'page-templates/parts/topbar', $layout );
	get_template_part( 'page-templates/parts/topbar', 'mobile' );
}

add_action( 'paradise_template_header_before', 'paradise_fnc_topbar_layout' );

/**
 * Hook to select header layout for archive layout
 */
function paradise_fnc_get_header_layout( $layout='' ){
	global $post; 

	$layout = $post && get_post_meta( $post->ID, 'paradise_header_layout', 1 ) ? get_post_meta( $post->ID, 'paradise_header_layout', 1 ) : paradise_fnc_theme_options( 'headerlayout' );
	if ($layout == 'default') {
		$layout = '';
	}
 	if( $layout ){
 		return trim( $layout );
 	}elseif ( $layout = paradise_fnc_theme_options('header_skin','') ){
 		return trim( $layout );
 	}

	return $layout;
} 

add_filter( 'paradise_fnc_get_header_layout', 'paradise_fnc_get_header_layout' );

/**
 * Hook to select header layout for archive layout
 */
function paradise_fnc_get_footer_profile( $profile='default' ){

	global $post; 

	$profile =  $post? get_post_meta( $post->ID, 'paradise_footer_profile', 1 ):null ;

 	if( $profile ){
 		return trim( $profile );
 	}elseif ( $profile = paradise_fnc_theme_options('footer-style', $profile ) ){  
 		return trim( $profile );
 	}

	return $profile;
} 

add_filter( 'paradise_fnc_get_footer_profile', 'paradise_fnc_get_footer_profile' );


/**
 * Render Custom Css Renderig by Visual composer
 */
if ( !function_exists( 'paradise_fnc_print_style_footer' ) ) {
	function paradise_fnc_print_style_footer(){
		$footer =  paradise_fnc_get_footer_profile( 'default' );
		if($footer!='default'){
			$shortcodes_custom_css = get_post_meta( $footer, '_wpb_shortcodes_custom_css', true );
			if ( ! empty( $shortcodes_custom_css ) ) {
				echo '<style>
					'.$shortcodes_custom_css.'
					</style>';
			}
		}
	}
	add_action('wp_head','paradise_fnc_print_style_footer', 18);
}


/**
 * Hook to show breadscrumbs
 */
function paradise_fnc_render_breadcrumbs(){
	
	global $post;
	$cls = '';
	if( is_object($post) && is_page() ){
		$disable = get_post_meta( $post->ID, 'paradise_disable_breadscrumb', 1 );  
		if(  $disable || is_front_page() ){
			return true; 
		}
		$bgimage = get_post_meta( $post->ID, 'paradise_image_breadscrumb', 1 );  
		$color 	 = get_post_meta( $post->ID, 'paradise_color_breadscrumb', 1 );  
		$bgcolor = get_post_meta( $post->ID, 'paradise_bgcolor_breadscrumb', 1 );  

		$style = array();

		if( $bgcolor  ){
			$style[] = 'background-color:'.$bgcolor;
		}

		if( $bgimage  ){ 
			$style[] = 'background-image:url(\''.wp_get_attachment_url($bgimage).'\')';
		}else{
			$bgimage = paradise_fnc_theme_options( 'breadcrumb-bg' );
			$style[] =  $bgimage  ? 'background-image:url(\''.$bgimage.'\')' : "";
		}

		if( $color  ){ 
			$style[] = 'color:'.$color;
		}

		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
		$cls .= ' '.$post->post_type;
	} else {

		$bgimage = paradise_fnc_theme_options( 'breadcrumb-bg' );
	 
		if( get_post_type() == 'post' ){
			if( is_single() ){  
				$bgimage =  paradise_fnc_theme_options( 'blog-single-breadcrumb' );
			}else {
				$bgimage =  paradise_fnc_theme_options( 'blog-archive-breadcrumb' );
			}
			if( !preg_match( '#'.get_home_url().'#' , $bgimage ) && $bgimage ){
				$bgimage = '';
			}
		}

		if( get_post_type() == 'opalhotel_room' ){
			if( is_single() ){  
				$bgimage =  paradise_fnc_theme_options( 'opalhotel-single-breadcrumb' );
			}else {
				$bgimage =  paradise_fnc_theme_options( 'opalhotel-archive-breadcrumb' );
			}
			if( !preg_match( '#'.get_home_url().'#' , $bgimage ) && $bgimage ){
				$bgimage = '';
			}
		}

		if( !empty($bgimage)  ){ 
			$style[] = 'background-image:url(\''.$bgimage.'\')';
		}
		$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	}



	echo '<section id="pbr-breadscrumb" class="pbr-breadscrumb'.$cls.'" '.$estyle.'><div class="container">';
		paradise_fnc_pagetitle();
	echo '</div></section>';
	echo '<section class="pbr-breadscrumb-inner"><div class="container">';		
		paradise_fnc_breadcrumbs();
	echo '</div></section>';

}

add_action( 'paradise_template_main_before', 'paradise_fnc_render_breadcrumbs' ); 

 
/**
 * Main Container
 */

function paradise_template_main_container_class( $class ){
	global $post; 
	global $paradise_wpopconfig;

	if(is_object( $post)){
		if( get_post_meta( $post->ID, 'paradise_enable_fullwidth_layout', 1 ) ){
			$paradise_wpopconfig['layout'] = 'fullwidth';
			return 'container-fluid';
		}
	}

	return $class;
}
add_filter( 'paradise_template_main_container_class', 'paradise_template_main_container_class', 1 , 1  );
add_filter( 'paradise_template_main_content_class', 'paradise_template_main_container_class', 1 , 1  );



function paradise_template_footer_before(){
	return get_sidebar( 'newsletter' );
}

add_action( 'paradise_template_footer_before', 'paradise_template_footer_before' );


/**
 * Get Configuration for Page Layout
 *
 */
function paradise_fnc_get_page_sidebar_configs( $configs='' ){

	global $post; 
	if ( is_object($post) ) {
		$left  	=  get_post_meta( $post->ID, 'paradise_leftsidebar', 1 );
		$right 	=  get_post_meta( $post->ID, 'paradise_rightsidebar', 1 );
		$layout =  get_post_meta( $post->ID, 'paradise_page_layout', 1 );
	} else {
		$left  	=  1;
		$right 	=  1;
		$layout =  1;
	}
	return paradise_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'paradise_fnc_get_page_sidebar_configs', 'paradise_fnc_get_page_sidebar_configs', 1, 1 );


function paradise_fnc_get_single_sidebar_configs( $configs='' ){

	global $post; 

	$layout =  paradise_fnc_theme_options( 'blog-single-layout', 'fullwidth');
	$left  	=  paradise_fnc_theme_options( 'blog-single-left-sidebar', 'blog-sidebar-left');
	$right 	=  paradise_fnc_theme_options( 'blog-single-right-sidebar', 'blog-sidebar-right');

	return paradise_fnc_get_layout_configs($layout, $left, $right, $configs );
}

add_filter( 'paradise_fnc_get_single_sidebar_configs', 'paradise_fnc_get_single_sidebar_configs', 1, 1 );

function paradise_fnc_get_archive_sidebar_configs( $configs='' ){

	$left  	=  paradise_fnc_theme_options( 'blog-archive-left-sidebar', 'blog-sidebar-left'); 
	$right 	=  paradise_fnc_theme_options( 'blog-archive-right-sidebar', 'blog-sidebar-right');
	$layout =  paradise_fnc_theme_options( 'blog-archive-layout', 'fullwidth'); 
 	
	return paradise_fnc_get_layout_configs($layout, $left, $right, $configs);
}

add_filter( 'paradise_fnc_get_archive_sidebar_configs', 'paradise_fnc_get_archive_sidebar_configs', 1, 1 );
add_filter( 'paradise_fnc_get_category_sidebar_configs', 'paradise_fnc_get_archive_sidebar_configs', 1, 1 );


function paradise_fnc_sidebars_others_configs(){
	
	global $paradise_page_layouts;
	
	return $paradise_page_layouts; 
}

add_filter("paradise_fnc_sidebars_others_configs", "paradise_fnc_sidebars_others_configs");


function paradise_fnc_get_layout_configs($layout, $left, $right, $configs= array()){

    switch ($layout) {
    
	    // Two Sidebar
	    case 'leftmainright':
	    	$configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-lg-4 col-md-4 col-md-pull-4 col-xs-12'
				),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-4'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-4 col-md-push-4');
	    break;

	    //One Sidebar Right
	    case 'mainright':
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array(
					'show'  	=> true,
					'sidebar' 	=> $right,
					'active' 	=> is_active_sidebar( $right ),
					'class' 	=> 'col-xs-12 col-md-4 pull-right'
				)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-8 pull-left');
	    break;

	    // One Sidebar Left
	    case 'leftmain':
	        $configs['sidebars'] = array(
	    		'left'  => array(
	    			'show'  	=> true, 
	    			'sidebar' 	=> $left, 
	    			'active'  	=> is_active_sidebar( $left ), 
	    			'class' 	=> 'col-xs-12 col-md-4'
				),
				'right' => array('show'  	=> false)
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-8 pull-right');
	    break;

	    // Fullwidth
	    default:
	        $configs['sidebars'] = array(
	    		'left'  => array('show'  	=> false),
				'right' => array('show'  	=> false )
			);
			$configs['main'] = array('class' => 'col-xs-12 col-md-12');
	        break;
	    }

    return $configs;
}

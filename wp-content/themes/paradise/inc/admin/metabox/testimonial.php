<?php
/**
 * Register meta boxes
 *
 * Remember to change "your_prefix" to actual prefix in your project
 *
 * @param array $meta_boxes List of meta boxes
 *
 * @return array
 */
function paradise_func_register_testimonial_meta_boxes( $meta_boxes )
{
	$prefix = 'paradise_testimonial_';
	$ratings = array(
		'1' => esc_html__('1 star', 'paradise'),
		'2' => esc_html__('2 stars', 'paradise'),
		'3' => esc_html__('3 stars', 'paradise'),
		'4' => esc_html__('4 stars', 'paradise'),
		'5' => esc_html__('5 stars', 'paradise'),
	);
	// 1st meta box
	$meta_boxes[] = array(
		// Meta box id, UNIQUE per meta box. Optional since 4.1.5
		'id'         => 'standard',
		// Meta box title - Will appear at the drag and drop handle bar. Required.
		'title'      => esc_html__( 'Testimonial Information', 'paradise' ),
		// Post types, accept custom post types as well - DEFAULT is 'post'. Can be array (multiple post types) or string (1 post type). Optional.
		'post_types' => array('testimonial' ),
		// Where the meta box appear: normal (default), advanced, side. Optional.
		'context'    => 'normal',
		// Order of meta box: high (default), low. Optional.
		'priority'   => 'high',
		// Auto save: true, false (default). Optional.
		'autosave'   => true,
		// List of meta fields
		'fields'     => array(
		 	array(
				'name' => esc_html__( 'Subject', 'paradise' ),
				'id'   => "{$prefix}subject",
				'type' => 'text',
				'std'  => '',
			),
			array(
	            'name' => esc_html__( 'Job', 'paradise' ),
	            'id'   => "{$prefix}job",
	            'type' => 'text',
	            'desc' => esc_html__('Enter Job example CEO, CTO','paradise')
          ), 		
			
		)
	);	
 	 
	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'paradise_func_register_testimonial_meta_boxes' , 102 );
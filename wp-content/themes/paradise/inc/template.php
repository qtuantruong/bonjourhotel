<?php 
/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since paradise 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function paradise_fnc_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	 
	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	$currentSkin = str_replace( '.css','',paradise_fnc_theme_options('skin','default') ); 
	
	if( $currentSkin ){
		$class[] = 'skin-'.$currentSkin;
	}

	return $classes;
}
add_filter( 'body_class', 'paradise_fnc_body_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since paradise 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function paradise_fnc_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( esc_html__( 'Page %s', 'paradise' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'paradise_fnc_wp_title', 10, 2 );

 
/**
 * upbootwp_breadcrumbs function.
 * Edit the standart breadcrumbs to fit the bootstrap style without producing more css
 * @access public
 * @return void
 */
function paradise_fnc_pagetitle() {

	$delimiter = '';
	$home = 'Home';
	$before = '<li class="active">';
	$after = '</li>';
	$title = '';
	$breadcrumbs_html = '';
	if (!is_home() && !is_front_page() || is_paged()) {

		global $post;

		if (is_category()) {
			global $wp_query;
			$title = esc_html__( 'Blogs', 'paradise' );
		} elseif (is_day()) {
			$title = get_the_time('d');
		} elseif (is_month()) {
			$title = get_the_time('F');
		} elseif (is_year()) {
			$title = get_the_time('Y');
		} elseif (is_single() && !is_attachment()) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				if ( get_post_type() == 'opalhotel_hotel' ) {
					$title  = esc_html__( 'Single Room', 'paradise' );
				} else {
					$title = get_the_title();
				}
			} else {
				$title  = esc_html__( 'Blog', 'paradise' );
			}
			
		} elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
			$post_type = get_post_type_object(get_post_type());
			if ( is_search() ) {
				$title = esc_html__( 'Search', 'paradise' );
			} else if( is_tax() ){
				$term = get_queried_object();
				$title = $term->name;
			} elseif ( is_object($post_type) ) {
				$title = $post_type->labels->singular_name;
			}
		} elseif (is_attachment()) {
			$title = get_the_title();
		} elseif ( is_page() && !$post->post_parent ) {
			$title = get_the_title();

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$title = get_the_title();
		} elseif ( is_search() ) {
			$title = esc_html__('Search results for "','paradise')  . get_search_query();
		} elseif ( is_tag() ) {
			$title = esc_html__('Posts tagged "', 'paradise'). single_tag_title('', false) . '"';
		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			$title = esc_html__('Articles posted by ', 'paradise') . $userdata->display_name;
		} elseif ( is_404() ) {
			$title = esc_html__('Error 404', 'paradise');
		}

		$breadcrumbs_html .= '</ol>';
		echo '<h3>'.$title.'</h3>';
	}
}

function paradise_fnc_breadcrumbs() {

	$delimiter = '';
	$home = 'Home';
	$before = '<li class="active">';
	$after = '</li>';
	$title = '';
	$breadcrumbs_html = '';
	if (!is_home() && !is_front_page() || is_paged()) {

		$breadcrumbs_html .= '<ol class="breadcrumb">';

		global $post;
		$homeLink = esc_url( home_url('/') );
		$breadcrumbs_html .= '<li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li> ';

		if (is_category()) {
			global $wp_query;
			$cat_obj = $wp_query->get_queried_object();
			$thisCat = $cat_obj->term_id;
			$thisCat = get_category($thisCat);
			$parentCat = get_category($thisCat->parent);
			if ($thisCat->parent != 0) $breadcrumbs_html .= (get_category_parents($parentCat, TRUE, ' ' . $delimiter . ' '));
			$breadcrumbs_html .= trim($before) . single_cat_title('', false) . $after;
			//$title = single_cat_title('', false);
			$title = esc_html__( 'Blogs', 'paradise' );
		} elseif (is_day()) {
			$breadcrumbs_html .= '<li><a href="' . esc_url( get_year_link(get_the_time('Y')) ) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			$breadcrumbs_html .= '<li><a href="' . esc_url( get_month_link(get_the_time('Y'),get_the_time('m')) ) . '">' . get_the_time('F') . '</a></li> ' . $delimiter . ' ';
			$breadcrumbs_html .= trim($before) . get_the_time('d') . $after;
			$title = get_the_time('d');
		} elseif (is_month()) {
			$breadcrumbs_html .= '<a href="' . esc_url( get_year_link(get_the_time('Y')) ) . '">' . get_the_time('Y') . '</a></li> ' . $delimiter . ' ';
			$breadcrumbs_html .= trim($before) . get_the_time('F') . $after;
			$title = get_the_time('F');
		} elseif (is_year()) {
			$breadcrumbs_html .= trim($before) . get_the_time('Y') . $after;
			$title = get_the_time('Y');
		} elseif (is_single() && !is_attachment()) {
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				$breadcrumbs_html .= '<li><a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a></li> ' . $delimiter . ' ';
				$breadcrumbs_html .= trim($before) . get_the_title() . $after;
				if ( get_post_type() == 'opalhotel_hotel' ) {
					$title  = esc_html__( 'Single Room', 'paradise' );
				} else {
					$title = get_the_title();
				}
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$breadcrumbs_html .= '<li>' . get_category_parents($cat, TRUE, ' ' . $delimiter . ' ') . '</li>';
				$breadcrumbs_html .= trim($before) . get_the_title() . $after;
				$title  = esc_html__( 'Blog', 'paradise' );
			}
			
		} elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
			$post_type = get_post_type_object(get_post_type());
			if ( is_search() ) {
				$title = esc_html__( 'Search', 'paradise' );
			}elseif (is_object($post_type)) {
				$breadcrumbs_html .= trim($before) . '<a href="'.get_post_type_archive_link( $post_type->name ).'">'.esc_html( $post_type->labels->singular_name ).'</a>' . $after;
				if ( is_tax() ){
					$term = get_queried_object();
					$breadcrumbs_html .= trim($before) . $term->name . $after;
				}
			}
		} elseif (is_attachment()) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			$breadcrumbs_html .= get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			$breadcrumbs_html .= '<a href="' . esc_url( get_permalink($parent) ) . '">' . $parent->post_title . '</a></li> ' . $delimiter . ' ';
			$breadcrumbs_html .= trim($before) . get_the_title() . $after;
			$title = get_the_title();
		} elseif ( is_page() && !$post->post_parent ) {
			$breadcrumbs_html .= trim($before) . get_the_title() . $after;
			$title = get_the_title();

		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . esc_url( get_permalink($page->ID) ) . '">' . get_the_title($page->ID) . '</a></li>';
				$parent_id  = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			foreach ($breadcrumbs as $crumb) $breadcrumbs_html .= trim($crumb) . ' ' . $delimiter . ' ';
			$breadcrumbs_html .= trim($before) . get_the_title() . $after;
			$title = get_the_title();
		} elseif ( is_search() ) {
			$breadcrumbs_html .= trim($before) . esc_html__('Search results for "','paradise')  . get_search_query() . '"' . $after;
			$title = esc_html__('Search results for "','paradise')  . get_search_query();
		} elseif ( is_tag() ) {
			$breadcrumbs_html .= trim($before) . esc_html__('Posts tagged "', 'paradise'). single_tag_title('', false) . '"' . $after;
			$title = esc_html__('Posts tagged "', 'paradise'). single_tag_title('', false) . '"';
		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			$breadcrumbs_html .= trim($before) . esc_html__('Articles posted by ', 'paradise') . $userdata->display_name . $after;
			$title = esc_html__('Articles posted by ', 'paradise') . $userdata->display_name;
		} elseif ( is_404() ) {
			$breadcrumbs_html .= trim($before) . esc_html__('Error 404', 'paradise') . $after;
			$title = esc_html__('Error 404', 'paradise');
		}

		$breadcrumbs_html .= '</ol>';
		echo $breadcrumbs_html;
	}
}

 

function paradise_fnc_categories_searchform(){
	get_search_form();
}

<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author      Team <opalwordpressl@gmail.com >
 * @copyright  Copyright (C) 2015  prestabrain.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
class Paradise_Copyright_Widget extends WP_Widget {
    public function __construct() {
        parent::__construct(
            // Base ID of your widget
            'paradise_copyright_widget',
            // Widget name will appear in UI
            esc_html__('Opal Copyright', 'paradise'),
            // Widget description
            array( 'description' => esc_html__( 'Opal Copyright', 'paradise' ), )
        );
    }

    /**
     * The main widget output function.
     * @param array $args
     * @param array $instance
     * @return string The widget output (html).
     */
    public function widget( $args, $instance ) {
        extract( $args, EXTR_SKIP );
        extract( $instance, EXTR_SKIP );
        $title = apply_filters( 'widget_title', $instance['title'] );
        echo trim($before_widget);
        ?>

        
        <section class="pbr-copyright">
            <div class="container">
                <a href="#" class="scrollup"><span class="fa fa-angle-up"></span></a>
                <div class="site-info">
                    <?php do_action( 'paradise_fnc_credits' ); ?>
                    <?php $copyright_text = paradise_fnc_theme_options('copyright_text', '');
                    if(!empty($copyright_text)){
                        echo wp_kses_post($copyright_text);
                    }else{
                        $devby = '<a target="_blank" href="'.esc_url('http://www.wpopal.com').'">WpOpal Team</a>';
                        printf( esc_html__( 'Proudly powered by %s. Developed by %s', 'paradise' ), 'WordPress', $devby );   
                    } ?>
                </div><!-- .site-info -->
            </div>  
        </section>
           
        <?php
        echo trim($after_widget);
    }

    /**
     * The function for saving widget updates in the admin section.
     * @param array $new_instance
     * @param array $old_instance
     * @return array The new widget settings.
     */
     
    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $new_instance = $this->default_instance_args( $new_instance );

        /* Strip tags (if needed) and update the widget settings. */
        $instance['title']   = $new_instance['title'];

        return $instance;
    }

    /**
     * Output the admin form for the widget.
     * @param array $instance
     * @return string The output for the admin widget form.
     */
    public function form( $instance ) {
        $instance  = $this->default_instance_args( $instance );

    ?>
    <div class="wpopal_recentpost">
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'paradise' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
        </p>  
    </div>
<?php
    }

    /**
     * Accepts and returns the widget's instance array - ensuring any missing
     * elements are generated and set to their default value.
     * @param array $instance
     * @return array
     */
    protected function default_instance_args( array $instance ) {
        return wp_parse_args( $instance, array(
            'title'   => esc_html__( 'Opal Copyright', 'paradise' ),
        ) );
    }

    public function display_footer_copyright(){
        
    }
}
register_widget( 'Paradise_Copyright_Widget' );
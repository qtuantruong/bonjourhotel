<?php 

class Paradise_VC_Elements implements Vc_Vendor_Interface {

	public function load(){ 
		
		/*********************************************************************************************************************
		 *  Featured Box
		 *********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Featured Box",'paradise'),
			"base" => "pbr_featuredbox",
			'icon' => 'icon-wpb-widgets-8',
			"description"=> esc_html__('Decreale Service Info', 'paradise'),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',    "admin_label" => true,
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Title Color', 'paradise' ),
					'param_name' => 'title_color',
					'description' => esc_html__( 'Select font color', 'paradise' )
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Sub Title", 'paradise'),
					"param_name" => "subtitle",
					"value" => '',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'paradise'),
					"param_name" => "style",
					'value' 	=> array(
						esc_html__('Default', 'paradise') => '', 
						esc_html__('Version 1', 'paradise') => 'v1', 
						esc_html__('Version 2', 'paradise') => 'v2', 
						esc_html__('Version 3', 'paradise' )=> 'v3',
						esc_html__('Version 4', 'paradise') => 'v4'
					),
					'std' => ''
				),

				array(
					'type'                           => 'dropdown',
					'heading'                        => esc_html__( 'Title Alignment', 'paradise' ),
					'param_name'                     => 'title_align',
					'value'                          => array(
					esc_html__( 'Align left', 'paradise' )   => 'separator_align_left',
					esc_html__( 'Align center', 'paradise' ) => 'separator_align_center',
					esc_html__( 'Align right', 'paradise' )  => 'separator_align_right'
					),
					'std' => 'separator_align_left'
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'paradise'),
					"param_name" => "icon",
					"value" => 'fa fa-gear',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'paradise' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'paradise' ) . '</a>'
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Icon Color', 'paradise' ),
					'param_name' => 'color',
					'description' => esc_html__( 'Select font color', 'paradise' )
				),	
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Background Icon', 'paradise' ),
					'param_name' => 'background',
					'value' => array(
						esc_html__( 'None', 'paradise' ) => 'nostyle',
						esc_html__( 'Success', 'paradise' ) => 'bg-success',
						esc_html__( 'Info', 'paradise' ) => 'bg-info',
						esc_html__( 'Danger', 'paradise' ) => 'bg-danger',
						esc_html__( 'Warning', 'paradise' ) => 'bg-warning',
						esc_html__( 'Light', 'paradise' ) => 'bg-default',
					),
					'std' => 'nostyle',
				),

				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'paradise'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'paradise'),
					"param_name" => "information",
					"value" => 'Your Description Here',
					'description'	=> esc_html__('Allow  put html tags', 'paradise' )
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));

		/*********************************************************************************************************************
		 *  Call To Action
		 *********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Call To Action",'paradise'),
			"base" => "pbr_cta",
			'icon' => 'icon-wpb-widgets-22',
			"description"=> esc_html__('Decleare Service Info', 'paradise'),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',    "admin_label" => true,
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Title Color', 'paradise' ),
					'param_name' => 'title_color',
					'description' => esc_html__( 'Select font color', 'paradise' )
				),

				array(
					'type'                           => 'dropdown',
					'heading'                        => esc_html__( 'Title Alignment', 'paradise' ),
					'param_name'                     => 'title_align',
					'value'                          => array(
					esc_html__( 'Align left', 'paradise' )   => 'separator_align_left',
					esc_html__( 'Align center', 'paradise' ) => 'separator_align_center',
					esc_html__( 'Align right', 'paradise' )  => 'separator_align_right'
					),
					'std' => 'separator_align_left'
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'paradise'),
					"param_name" => "icon",
					"value" => 'fa fa-gear',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'paradise' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'paradise' ) . '</a>'
				),
				array(
					'type' => 'colorpicker',
					'heading' => esc_html__( 'Icon Color', 'paradise' ),
					'param_name' => 'color',
					'description' => esc_html__( 'Select font color', 'paradise' )
				),

				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'paradise'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),

				array(
					'type' => 'vc_link',
					'heading' => __( 'URL (Link)', 'paradise' ),
					'param_name' => 'link',
					'description' => __( 'Add link to title.', 'paradise' )
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));
		 
		/*********************************************************************************************************************
		 * Pricing Table
		 *********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Pricing",'paradise'),
			"base" => "pbr_pricing",
			'icon' => 'icon-wpb-widgets-9',
			"description" => esc_html__('Make Plan for membership', 'paradise' ),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					'type' => 'param_group',
					'heading' => esc_html__('Pricing Plans', 'paradise' ),
					'param_name' => 'items',
					'description' => '',
					'value' => '',
					'params' => array(
						array(
							"type" => "textfield",
							"heading" => esc_html__("Title", 'paradise'),
							"param_name" => "title",
							"value" => '',
								"admin_label" => true
						),
						array(
							"type" => "textfield",
							"heading" => esc_html__("Subtitle", 'paradise'),
							"param_name" => "subtitle",
							"value" => '',
							'description'	=> ''
						),
						array(
							"type" => "textfield",
							"heading" => esc_html__("Price", 'paradise'),
							"param_name" => "price",
							"value" => '',
							'description'	=> ''
						),
						array(
							"type" => "textfield",
							"heading" => esc_html__("Currency", 'paradise'),
							"param_name" => "currency",
							"value" => '',
							'description'	=> ''
						),
					),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));
		

		/*********************************************************************************************************************
		 *  PBR Counter
		 *********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Counter",'paradise'),
			"base" => "pbr_counter",
			'icon' => 'icon-wpb-widgets-10',
			"class" => "",
			"description"=> esc_html__('Counting number with your term', 'paradise'),
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',
					"admin_label"	=> true
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("Description", 'paradise'),
					"param_name" => "description",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number", 'paradise'),
					"param_name" => "number",
					"value" => ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("FontAwsome Icon", 'paradise'),
					"param_name" => "icon",
					"value" => '',
					'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'paradise' )
									. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
									. esc_html__( 'here to see the list', 'paradise' ) . '</a>'
				),


				array(
					"type" => "attach_image",
					"description" => esc_html__("If you upload an image, icon will not show.", 'paradise'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> esc_html__('Image', 'paradise' )
				),

		 

				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Text Color", 'paradise'),
					"param_name" => "text_color",
					'value' 	=> '',
				),

				array(
					"type" => "colorpicker",
					"heading" => esc_html__("Number Color", 'paradise'),
					"param_name" => "number_color",
					'value' 	=> '',
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));


		/**************************************************
		*   Element time line 
		**************************************************/
		vc_map( array(
				'name'        => esc_html__('PBR Timeline','paradise'),
				'base'        => 'pbr_timeline',
				'icon' => 'icon-wpb-widgets-11',
				"class"       => "",
				"category" => esc_html__('PBR Widgets', 'paradise'),
				'description' => esc_html__('Create Item timeline with content + icon', 'paradise' ),
				'params'		=> array(
					array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
					),

					array(
						"type" => "textfield",
						"heading" => esc_html__("Sub Title", 'paradise'),
						"param_name" => "subtitle",
						"value" => '',
							"admin_label" => true
					),
					array(
						'type' => 'dropdown',
						'heading' => esc_html__('Title Alignment', 'paradise' ),
						'param_name' => 'alignment',
						'value' => array(
							esc_html__('Align left', 'paradise' ) => 'separator_align_left',
							esc_html__('Align center', 'paradise' ) => 'separator_align_center',
							esc_html__('Align right', 'paradise' ) => 'separator_align_right'
						)
					),
					
					array(
						'type' => 'param_group',
						'heading' => esc_html__('Items', 'paradise' ),
						'param_name' => 'items',
						'description' => '',
						'value' => urlencode( json_encode( array(
							
						) ) ),

						'params' => array(
							array(
								'type' => 'textfield',
								'heading' => esc_html__('Title', 'paradise' ),
								'param_name' => 'title',
								'admin_label' => true,
							),
							array(
								'type' => 'textfield',
								'heading' => esc_html__('Sub Title', 'paradise' ),
								'param_name' => 'sub_title',
								'admin_label' => true,
							),
							array(
								'type' => 'attach_image',
								'heading' => esc_html__('Icon', 'paradise' ),
								'param_name' => 'icon',
								'admin_label' => false,
							),
							array(
								'type' => 'textarea',
								'heading' => esc_html__('Content', 'paradise' ),
								'param_name' => 'content',
								'admin_label' => false,
							),
						),
					),
				)
			)
		);


		/* Gallery
		***************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Gallery",'paradise'),
			"base" => "pbr_gallery",
			'icon' => 'icon-wpb-widgets-12',
			'description'=> esc_html__('Diplay Gallery Grid', 'paradise'),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => '',
						"admin_label" => true
				),
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Number', 'paradise' ),
				'param_name' => 'number',
				'value' => '',
			),
			array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns", 'paradise'),
					"param_name" => "columns",
					"value" => array(6, 4, 3, 1),
				),
			array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Enable Pagination', 'paradise' ),
						'param_name' => 'pagination',
						'value' => array( 'No'=>'0', 'Yes'=>'1'),
						'std' => 'style-1',
						'admin_label' => true,
						'description' => esc_html__( 'Show paginaion.', 'paradise' )
					  ),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   )
		));


		/* Video
		***************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Video",'paradise'),
			"base" => "pbr_video",
			'icon' => 'icon-wpb-widgets-18',
			'description'=> esc_html__('Diplay Video Grid', 'paradise'),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => '',
						"admin_label" => true
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Number', 'paradise' ),
					'param_name' => 'number',
					'value' => '',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns", 'paradise'),
					"param_name" => "columns",
					"value" => array(6, 4, 3, 1),
				),
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Enable Pagination', 'paradise' ),
					'param_name' => 'pagination',
					'value' => array( 'No'=>'0', 'Yes'=>'1'),
					'std' => 'style-1',
					'admin_label' => true,
					'description' => esc_html__( 'Show paginaion.', 'paradise' )
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));


		/* Contact Information
		**********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Contact Information", 'paradise'),
			"base" => "pbr_contact_information",
			'icon' => 'icon-wpb-widgets-17',
			'description'=> esc_html__('Diplay Contact information', 'paradise'),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Address', 'paradise' ),
					'param_name' => 'address',
					'value' => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Hot line", 'paradise'),
					"param_name" => "phone",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Fax", 'paradise'),
					"param_name" => "fax",
					"value" => '',
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Email", 'paradise'),
					"param_name" => "email",
					"value" => '',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'paradise'),
					"param_name" => "style",
					'value' 	=> array( 'default' => esc_html__('default', 'paradise') , 'style-2' => esc_html__('style-2', 'paradise') ),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));


		// --------- PBR Content Carousel -----------------------------
		vc_map( array(
			"name" => esc_html__("PBR Content Carousel", 'paradise'),
			"base" => "pbr_content_carousel",
			'icon' => 'icon-wpb-widgets-16',
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"description"	=> esc_html__( 'Display content carousel', 'paradise' ),
			"params" => array(
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_html__('Title', 'paradise'),
					"param_name" => "title",
				),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_html__('Sub Title', 'paradise'),
					"param_name" => "subtitle",
				),
				array(
					'type' => 'param_group',
					'heading' => esc_html__('Carousel Items', 'paradise' ),
					'param_name' => 'items',
					'description' => '',
					'value' => '',
					'params' => array(
						array(
					   "type" => "textfield",
					   "class" => "",
					   "heading" => esc_html__('Title', 'paradise'),
					   "param_name" => "title",
						"admin_label" => true
					  ),
						array(
							"type" => "textarea",
							"heading" => esc_html__("Description", 'paradise'),
							"param_name" => "description",
							"value" => '',
						),

					),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));


		/* Video background
		**********************************************************************************************************************/
		vc_map( array(
	        "name" => esc_html__("PBR Video Background",'paradise'),
	        "base" => "pbr_video_carousel",
			'icon' => 'icon-wpb-widgets-13',
	        "class" => "",
	    	"category" => esc_html__('PBR Widgets','paradise'),
	    	"description"	=> esc_html__( 'Display video carousel', 'paradise' ),
	        "params" => array(
	        	array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => esc_html__('Title','paradise'),
	                "param_name" => "title",
	            ),
            	array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => esc_html__('Sub Title','paradise'),
	                "param_name" => "subtitle",
	            ),
            	array(
					'type' => 'param_group',
					'heading' => esc_html__('Carousel Items', 'paradise' ),
					'param_name' => 'items',
					'description' => '',
					'value' => '',
					'params' => array(
						array(
			                "type" => "textfield",
			                "class" => "",
			                "heading" => esc_html__('Title','paradise'),
			                "param_name" => "title",
			            ),
						array(
							"type" => "attach_image",
							"heading" => esc_html__("Image", 'paradise'),
							"param_name" => "image"
						),
						array(
			                "type" => "textfield",
			                "class" => "",
			                "heading" => esc_html__('Video Link','paradise'),
			                "param_name" => "video_link",
			                "description" => esc_html__('Enter Youtube, Vimeo video url', 'paradise')
			            ),

					),
				),
	            array(
	                "type" => "textfield",
	                "heading" => esc_html__("Extra class name", 'paradise'),
	                "param_name" => "el_class",
	                "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
	            )
        	)
	    ));


	    /*********************************************************************************************************************
		 * Pricing Table
		 *********************************************************************************************************************/
		vc_map( array(
			"name" => esc_html__("PBR Openning Time",'paradise'),
			"base" => "pbr_opentime",
			'icon' => 'icon-wpb-widgets-19',
			"description" => esc_html__('Make Plan for openning time', 'paradise' ),
			"class" => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			"params" => array(
				array(
					'type' => 'param_group',
					'heading' => esc_html__('Openning Plans', 'paradise' ),
					'param_name' => 'items',
					'description' => '',
					'value' => '',
					'params' => array(
						array(
							"type" => "textfield",
							"heading" => esc_html__("Date", 'paradise'),
							"param_name" => "date",
							"value" => '',
								"admin_label" => true
						),
						array(
							"type" => "textfield",
							"heading" => esc_html__("Time", 'paradise'),
							"param_name" => "time",
							"value" => '',
							'description'	=> ''
						),
					),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
			)
		));


		/* Testimonials Carousel Masonry
		**********************************************************************************************************************/
		vc_map( array(
		    "name" => esc_html__("PBR Testimonials Masonry", 'paradise'),
		    "base" => "pbr_testimonials_masonry",
		    'icon' => 'icon-wpb-widgets-20',
		    'description'=> esc_html__('Display Testimonials In Masonry', 'paradise'),
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"admin_label" => true,
					"value" => ''
				),
	         array(
					"type" => "textfield",
					"heading" => esc_html__("Number", 'paradise'),
					"param_name" => "number",
					"value" => '9',
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns", 'paradise'),
					"param_name" => "columns",
					"value" => array(2, 3, 4, 6),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   )
		));


		/* Medias Tab
		**********************************************************************************************************************/
		
		vc_map( array(
		    "name" => esc_html__("PBR Medias Tabs",'paradise'),
		    "base" => "pbr_tabs_medias",
		    'icon' => 'icon-wpb-widgets-21',
		    'description'	=> esc_html__( 'Display Gallery, Video In tabs', 'paradise' ),
		    "class" => "",
		    "category" => esc_html__('PBR Widgets','paradise'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title",'paradise'),
					"param_name" => "title",
					"value" => ''
				),
				 array(
	                "type" => "textfield",
	                "class" => "",
	                "heading" => esc_html__('Sub Title','paradise'),
	                "param_name" => "subtitle",
	            ),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Number of media to show",'paradise'),
					"param_name" => "number",
					"value" => '4'
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Columns", 'paradise'),
					"param_name" => "columns",
					"value" => array(6, 4, 3, 1),
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name",'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.",'paradise')
				)
		   	)
		));
		
	}
}
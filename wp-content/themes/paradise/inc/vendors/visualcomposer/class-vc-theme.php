<?php 

class Paradise_VC_Theme implements Vc_Vendor_Interface {

	public function load(){
		/*********************************************************************************************************************
		 *  Vertical menu
		 *********************************************************************************************************************/
		$option_menu  = array(); 
		if( is_admin() ){
			$menus = wp_get_nav_menus( array( 'orderby' => 'name' ) );
		    $option_menu = array('---Select Menu---'=>'');
		    foreach ($menus as $menu) {
		    	$option_menu[$menu->name]=$menu->term_id;
		    }
		}    
		vc_map( array(
		    "name" => esc_html__("PBR Quick Links Menu",'paradise'),
		    "base" => "pbr_quicklinksmenu",
'icon' => 'icon-wpb-widgets-1',
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    'description'	=> esc_html__( 'Show Quick Links To Access', 'paradise'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => 'Quick To Go'
				),
		    	array(
					"type" => "dropdown",
					"heading" => esc_html__("Menu", 'paradise'),
					"param_name" => "menu",
					"value" => $option_menu,
					"description" => esc_html__("Select menu.", 'paradise')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   	)
		));
		 
		

		/*********************************************************************************************************************
		 *  Vertical menu
		 *********************************************************************************************************************/
	 
		vc_map( array(
		    "name" => esc_html__("PBR Vertical MegaMenu",'paradise'),
		    "base" => "pbr_verticalmenu",
'icon' => 'icon-wpb-widgets-2',
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    "params" => array(

		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => 'Vertical Menu',
					"admin_label"	=> true
				),

		    	array(
					"type" => "dropdown",
					"heading" => esc_html__("Menu", 'paradise'),
					"param_name" => "menu",
					"value" => $option_menu,
					"description" => esc_html__("Select menu.", 'paradise')
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Position", 'paradise'),
					"param_name" => "postion",
					"value" => array(
							'left'=>'left',
							'right'=>'right'
						),
					'std' => 'left',
					"description" => esc_html__("Postion Menu Vertical.", 'paradise')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   	)
		));
		 
		vc_map( array(
		    "name" => esc_html__("Fixed Show Vertical Menu ",'paradise'),
		    "base" => "pbr_verticalmenu_show",
'icon' => 'icon-wpb-widgets-3',
		    "class" => "",
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    "description" => esc_html__( 'Always showing vertical menu on top', 'paradise' ),
		    "params" => array(
		  
				array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"description" => esc_html__("When enabled vertical megamenu widget on main navition and its menu content will be showed by this module. This module will work with header:Martket, Market-V2, Market-V3" , 'paradise')
				)
		   	)
		));
	 

		/******************************
		 * Our Team
		 ******************************/
		vc_map( array(
		    "name" => esc_html__("PBR Our Team Grid Style",'paradise'),
		    "base" => "pbr_team",
'icon' => 'icon-wpb-widgets-4',
		    "class" => "",
		    "description" => 'Show Personal Profile Info',
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'paradise'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Job", 'paradise'),
					"param_name" => "job",
					"value" => 'CEO',
					'description'	=>  ''
				),

				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'paradise'),
					"param_name" => "information",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'paradise')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'paradise'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Email", 'paradise'),
					"param_name" => "email",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Google Plus", 'paradise'),
					"param_name" => "google",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Facebook", 'paradise'),
					"param_name" => "facebook",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Twitter", 'paradise'),
					"param_name" => "twitter",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Pinterest", 'paradise'),
					"param_name" => "pinterest",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Youtube", 'paradise'),
					"param_name" => "youtube",
					"value" => '',
					'description'	=> ''
				),

				array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'paradise'),
					"param_name" => "style",
					'value' 	=> array( 'vertical' => esc_html__('vertical', 'paradise') , 'special' => esc_html__('special', 'paradise') ),
				),

				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   	)
		));
	 
		
		/******************************
		 * Information Reservations
		 ******************************/
		vc_map( array(
		    "name" => esc_html__("PBR Information Reservations",'paradise'),
		    "base" => "pbr_info_reservation",
'icon' => 'icon-wpb-widgets-5',
		    "class" => "",
		    "description" => 'Show Reservations Info',
		    "category" => esc_html__('PBR Widgets', 'paradise'),
		    "params" => array(
		    	array(
					"type" => "textfield",
					"heading" => esc_html__("Title", 'paradise'),
					"param_name" => "title",
					"value" => '',
						"admin_label" => true
				),
				array(
					"type" => "attach_image",
					"heading" => esc_html__("Photo", 'paradise'),
					"param_name" => "photo",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Phone", 'paradise'),
					"param_name" => "phone",
					"value" => '',
					'description'	=> ''
				),
				array(
					"type" => "textarea",
					"heading" => esc_html__("information", 'paradise'),
					"param_name" => "information",
					"value" => '',
					'description'	=> esc_html__('Allow  put html tags', 'paradise')
				),
				array(
					"type" => "textfield",
					"heading" => esc_html__("Extra class name", 'paradise'),
					"param_name" => "el_class",
					"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
				)
		   	)
		));
	 

		/* Heading Text Block
		---------------------------------------------------------- */
		vc_map( array(
			'name'        => esc_html__( 'PBR Widget Heading','paradise'),
			'base'        => 'pbr_title_heading',
'icon' => 'icon-wpb-widgets-6',
			"class"       => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			'description' => esc_html__( 'Create title for one Widget', 'paradise' ),
			"params"      => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget title', 'paradise' ),
					'param_name' => 'title',
					'value'       => esc_html__( 'Title', 'paradise' ),
					'description' => esc_html__( 'Enter heading title.', 'paradise' ),
					"admin_label" => true
				),
				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'paradise' ),
				    'param_name' => 'font_color',
				    'description' => esc_html__( 'Select font color', 'paradise' )
				),
				 
				array(
					"type" => "textarea",
					'heading' => esc_html__( 'Description', 'paradise' ),
					"param_name" => "descript",
					"value" => '',
					'description' => esc_html__( 'Enter description for title.', 'paradise' )
			    ),
			    array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Description Color', 'paradise' ),
				    'param_name' => 'desc_color',
				    'description' => esc_html__( 'Select font color', 'paradise' )
				),

			    array(
					"type" => "dropdown",
					"heading" => esc_html__("Style", 'paradise'),
					"param_name" => "style",
					'value' 	=> array( 'default' => esc_html__('default', 'paradise'), 'style-2' => esc_html__('style-2', 'paradise'), 'style-3' => esc_html__('style-3', 'paradise'), 'style-4' => esc_html__('style-4', 'paradise'), 'style-5' => esc_html__('style-5', 'paradise'), 'style-6' => esc_html__('style-6', 'paradise'), 'style-7' => esc_html__('style-7', 'paradise'), 'style-8' => esc_html__('style-8', 'paradise'), 'style-9' => esc_html__('style-9', 'paradise') ),
				),
				array(
					"type" => "dropdown",
					"heading" => esc_html__("Text Align", 'paradise'),
					"param_name" => "text_align",
					'value' 	=> array( 'Left' => esc_html__('default', 'paradise'), 'Center' => esc_html__('text-center', 'paradise'), 'Right' => esc_html__('text-right', 'paradise') ),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'paradise' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'paradise' )
				)
			),
		));
		
		/* Banner CountDown
		---------------------------------------------------------- */
		vc_map( array(
			'name'        => esc_html__( 'PBR Banner CountDown','paradise'),
			'base'        => 'pbr_banner_countdown',
'icon' => 'icon-wpb-widgets-7',
			"class"       => "",
			"category" => esc_html__('PBR Widgets', 'paradise'),
			'description' => esc_html__( 'Show CountDown with banner', 'paradise' ),
			"params"      => array(
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Widget title', 'paradise' ),
					'param_name' => 'title',
					'value'       => esc_html__( 'Title', 'paradise' ),
					'description' => esc_html__( 'Enter heading title.', 'paradise' ),
					"admin_label" => true
				),


				array(
					"type" => "attach_image",
					"description" => esc_html__("If you upload an image, icon will not show.", 'paradise'),
					"param_name" => "image",
					"value" => '',
					'heading'	=> esc_html__('Image', 'paradise' )
				),


				array(
				    'type' => 'textfield',
				    'heading' => esc_html__( 'Date Expired', 'paradise' ),
				    'param_name' => 'input_datetime',
				    'description' => esc_html__( 'Select font color', 'paradise' ),
				),
				 

				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Title Color', 'paradise' ),
				    'param_name' => 'font_color',
				    'description' => esc_html__( 'Select font color', 'paradise' ),
				    'class'	=> 'hacongtien'
				),
				 
				array(
					"type" => "textarea",
					'heading' => esc_html__( 'Description', 'paradise' ),
					"param_name" => "descript",
					"value" => '',
					'description' => esc_html__( 'Enter description for title.', 'paradise' )
			    ),

			    array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Description Color', 'paradise' ),
				    'param_name' => 'description_font_color',
				    'description' => esc_html__( 'Select font color', 'paradise' ),
				    'class'	=> 'hacongtien'
				),

				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Extra class name', 'paradise' ),
					'param_name' => 'el_class',
					'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'paradise' )
				),


				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Text Link', 'paradise' ),
					'param_name' => 'text_link',
					'value'		 => 'Find Out More',
					'description' => esc_html__( 'Enter your link text', 'paradise' )
				),

				array(
				    'type' => 'colorpicker',
				    'heading' => esc_html__( 'Link Color', 'paradise' ),
				    'param_name' => 'link_font_color',
				    'description' => esc_html__( 'Select font color', 'paradise' ),
				    'class'	=> 'hacongtien'
				),
				
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Link', 'paradise' ),
					'param_name' => 'link',
					'value'		 => 'http://',
					'description' => esc_html__( 'Enter your link to redirect', 'paradise' )
				)
			),
		));


	}
}
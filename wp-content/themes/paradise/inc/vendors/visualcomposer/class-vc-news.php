<?php 
class Paradise_VC_News implements Vc_Vendor_Interface  {
	
	public function load(){
		 
		$newssupported = true; 
 
			// front page 4
			vc_map( array(
				'name' => esc_html__( '(News) FrontPage 4', 'paradise' ),
				'base' => 'pbr_frontpageposts4',
				'icon' => 'icon-wpb-news-4',
				"category" => esc_html__('PBR News', 'paradise'),
				'description' => esc_html__( 'Create Post having blog styles', 'paradise' ),
				 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'paradise' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'paradise' ),
						"admin_label" => true
					),

				 
				 

					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'paradise' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'paradise' )
					),
					 
					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'paradise' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'paradise' ),
						'value' => array( esc_html__( 'Yes, please', 'paradise' ) => 'yes' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'paradise' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'paradise' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'paradise' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'paradise' )
					)
				)
			) );
			
			
			

			$layout_image = array(
				esc_html__('Grid', 'paradise')             => 'grid-1',
				esc_html__('List', 'paradise')             => 'list-1',
				esc_html__('List not image', 'paradise')   => 'list-2',
			);
			
			vc_map( array(
				'name' => esc_html__( '(News) Grid Posts', 'paradise' ),
				'base' => 'pbr_gridposts',
				'icon' => 'icon-wpb-news-2',
				"category" => esc_html__('PBR News', 'paradise'),
				'description' => esc_html__( 'Post having news,managzine style', 'paradise' ),
			 
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Widget title', 'paradise' ),
						'param_name' => 'title',
						'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'paradise' ),
						"admin_label" => true
					),
 
				 
					array(
						'type' => 'loop',
						'heading' => esc_html__( 'Grids content', 'paradise' ),
						'param_name' => 'loop',
						'settings' => array(
							'size' => array( 'hidden' => false, 'value' => 4 ),
							'order_by' => array( 'value' => 'date' ),
						),
						'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'paradise' )
					),
					array(
						"type" => "dropdown",
						"heading" => esc_html__("Layout Type", 'paradise'),
						"param_name" => "layout",
						"layout_images" => $layout_image,
						"value" => $layout_image,
						"admin_label" => true,
						"description" => esc_html__("Select Skin layout.", 'paradise')
					),

					array(
						'type' => 'checkbox',
						'heading' => esc_html__( 'Show Pagination Links', 'paradise' ),
						'param_name' => 'show_pagination',
						'description' => esc_html__( 'Enables to show paginations to next new page.', 'paradise' ),
						'value' => array( esc_html__( 'Yes, please', 'paradise' ) => 'yes' )
					),

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Grid Columns", 'paradise'),
						"param_name" => "grid_columns",
						"value" => array( 1 , 2 , 3 , 4 , 6),
						"std" => 3
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Thumbnail size', 'paradise' ),
						'param_name' => 'thumbsize',
						'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'paradise' )
					),
					array(
						'type' => 'textfield',
						'heading' => esc_html__( 'Extra class name', 'paradise' ),
						'param_name' => 'el_class',
						'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'paradise' )
					)
				)
			) );


			
			
		 

			/**********************************************************************************
			 * Slideshow Post Widget Gets
			 **********************************************************************************/
				vc_map( array(
					'name' => esc_html__( '(News) Slideshow Post', 'paradise' ),
					'base' => 'pbr_slideshopbrst',
					'icon' => 'icon-wpb-news-slideshow',
					"category" => esc_html__('PBR News', 'paradise'),
					'description' => esc_html__( 'Play Posts In slideshow', 'paradise' ),
					 
					'params' => array(
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Widget title', 'paradise' ),
							'param_name' => 'title',
							'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'paradise' ),
							"admin_label" => true
						),

					 

						array(
							'type' => 'textarea',
							'heading' => esc_html__( 'Heading Description', 'paradise' ),
							'param_name' => 'descript',
							"value" => ''
						),

						array(
							'type' => 'loop',
							'heading' => esc_html__( 'Grids content', 'paradise' ),
							'param_name' => 'loop',
							'settings' => array(
								'size' => array( 'hidden' => false, 'value' => 10 ),
								'order_by' => array( 'value' => 'date' ),
							),
							'description' => esc_html__( 'Create WordPress loop, to populate content from your site.', 'paradise' )
						),

						array(
							"type" => "dropdown",
							"heading" => esc_html__("Grid Columns", 'paradise'),
							"param_name" => "grid_columns",
							"value" => array( 1 , 2 , 3 , 4 , 6),
							"std" => 3
						),
						array(
							'type' => 'checkbox',
							'heading' => esc_html__( 'Show Pagination Links', 'paradise' ),
							'param_name' => 'show_pagination',
							'description' => esc_html__( 'Enables to show paginations to next new page.', 'paradise' ),
							'value' => array( esc_html__( 'Yes, please', 'paradise' ) => 'yes' )
						),

						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Thumbnail size', 'paradise' ),
							'param_name' => 'thumbsize',
							'description' => esc_html__( 'Enter thumbnail size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height) . ', 'paradise' )
						),
						array(
							'type' => 'textfield',
							'heading' => esc_html__( 'Extra class name', 'paradise' ),
							'param_name' => 'el_class',
							'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'paradise' )
						)
					)
				) );
 
	}
}
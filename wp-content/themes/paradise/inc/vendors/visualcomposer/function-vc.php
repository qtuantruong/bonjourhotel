<?php

 /**
  * Register Woocommerce Vendor which will register list of shortcodes
  */
function paradise_fnc_init_vc_vendors(){
	
	$vendor = new Paradise_VC_News();
	add_action( 'vc_after_set_mode', array(
		$vendor,
		'load'
	), 99 );


	$vendor = new Paradise_VC_Theme();
	add_action( 'vc_after_set_mode', array(
		$vendor,
		'load'
	), 99 );

	$vendor = new Paradise_VC_Elements();
	add_action( 'vc_after_set_mode', array(
		$vendor,
		'load'
	), 99 );


}
add_action( 'after_setup_theme', 'paradise_fnc_init_vc_vendors' , 99 );

/**
 * Add parameters for row
 */
function paradise_fnc_add_params(){

 	/**
	 * add new params for row
	 */
	vc_add_param( 'vc_row', array(
	    "type" => "checkbox",
	    "heading" => esc_html__("Parallax", 'paradise'),
	    "param_name" => "parallax",
	    "value" => array(
	        'Yes, please' => true
	    )
	));

	$row_class =  array(
        'type' => 'dropdown',
        'heading' => esc_html__( 'Background Styles', 'paradise' ),
        'param_name' => 'bgstyle',
        'description'	=> esc_html__('Use Styles Supported In Theme, Select No Use For Customizing on Tab Design Options','paradise'),
        'value' => array(
			esc_html__( 'No Use', 'paradise' ) => '',
			esc_html__( 'Background Color Primary', 'paradise' ) => 'bg-primary',
			esc_html__( 'Background Color Info', 'paradise' ) 	 => 'bg-info',
			esc_html__( 'Background Color Danger', 'paradise' )  => 'bg-danger',
			esc_html__( 'Background Color Warning', 'paradise' ) => 'bg-warning',
			esc_html__( 'Background Color Success', 'paradise' ) => 'bg-success',
			esc_html__( 'Background Color Theme', 'paradise' ) 	 => 'bg-theme',
			esc_html__( 'Background Color Navy', 'paradise' ) 	 => 'bg-navy',
		    esc_html__( 'Background Image 1 Dark', 'paradise' ) => 'bg-style-v1',
			esc_html__( 'Background Image 2 Dark', 'paradise' ) => 'bg-style-v2',
			esc_html__( 'Background Image 3 Blue', 'paradise' ) => 'bg-style-v3',
			esc_html__( 'Background Image 4 Red', 'paradise' ) => 'bg-style-v4',
        )
    ) ;

	vc_add_param( 'vc_row', $row_class );
	vc_add_param( 'vc_row_inner', $row_class );
 

	 vc_add_param( 'vc_row', array(
	     "type" => "dropdown",
	     "heading" => esc_html__("Is Boxed", 'paradise'),
	     "param_name" => "isfullwidth",
	     "value" => array(
	     				esc_html__('Yes, Boxed', 'paradise') => '1',
	     				esc_html__('No, Wide', 'paradise') => '0'
	     			)
	));

	vc_add_param( 'vc_row', array(
	    "type" => "textfield",
	    "heading" => esc_html__("Icon", 'paradise'),
	    "param_name" => "icon",
	    "value" => '',
		'description'	=> esc_html__( 'This support display icon from FontAwsome, Please click', 'paradise' )
						. '<a href="' . ( is_ssl()  ? 'https' : 'http') . '://fortawesome.github.io/Font-Awesome/" target="_blank">'
						. esc_html__( 'here to see the list, and use class icons-lg, icons-md, icons-sm to change its size', 'paradise' ) . '</a>'
	));
	// add param for image elements

	 vc_add_param( 'vc_single_image', array(
	     "type" => "textarea",
	     "heading" => esc_html__("Image Description", 'paradise'),
	     "param_name" => "description",
	     "value" => "",
	     'priority'	
	));
}
add_action( 'after_setup_theme', 'paradise_fnc_add_params', 99 );
 
 /** 
  * Replace pagebuilder columns and rows class by bootstrap classes
  */
function paradise_wpo_change_bootstrap_class( $class_string,$tag ){
 
	if ($tag=='vc_column' || $tag=='vc_column_inner') {
		$class_string = preg_replace('/vc_span(\d{1,2})/', 'col-md-$1', $class_string);
		$class_string = preg_replace('/vc_hidden-(\w)/', 'hidden-$1', $class_string);
		$class_string = preg_replace('/vc_col-(\w)/', 'col-$1', $class_string);
		$class_string = str_replace('wpb_column', '', $class_string);
		$class_string = str_replace('column_container', '', $class_string);
	}
	return $class_string;
}

add_filter( 'vc_shortcodes_css_class', 'paradise_wpo_change_bootstrap_class',10,2);

function paradise_video_display() {
	$video_link = isset($_POST['video_link']) ? $_POST['video_link'] : '';
	if ( !empty($video_link) ) {
		echo wp_oembed_get( $video_link, array('height' => '530px', 'width' => '1920px') );
	} else {
		echo '';
	}
	exit;
}
add_action( 'wp_ajax_video_display', 'paradise_video_display' );
add_action( 'wp_ajax_nopriv_video_display', 'paradise_video_display' );

function paradise_youtube_embed_url($html) {
    return str_replace("?feature=oembed", "?feature=oembed&autoplay=1", $html);
}
add_filter('oembed_result', 'paradise_youtube_embed_url');
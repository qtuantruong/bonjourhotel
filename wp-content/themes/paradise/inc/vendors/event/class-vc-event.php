<?php 
if( class_exists("WPBakeryShortCode") ){
	/**
	 * Class Paradise_VC_Woocommerces
	 *
	 */
	class Paradise_VC_Event  implements Vc_Vendor_Interface  {

		public function load() {
			$columns = array(1, 2, 3, 4, 6);
		    vc_map( array(
			    "name" => esc_html__("PBR Events",'paradise'),
			    "base" => "pbr_events",
			    "icon" => 'icon-wpb-event-1',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('PBR Event', 'paradise'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'paradise'),
						"param_name" => "title",
						"value" => '',
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Events', 'paradise') => 'featured',
							esc_html__('Lastest Events', 'paradise') => 'most_recent',
							esc_html__('Randown Events', 'paradise') => 'random'
						)
				    ),
					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						'type' => 'checkbox',
						'heading' => __( 'Show Image', 'paradise' ),
						'param_name' => 'show_image',
						'value' => array( __( 'Yes', 'paradise' ) => 'yes' )
					),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));
			
			vc_map( array(
			    "name" => esc_html__("PBR Events Carousel",'paradise'),
			    "base" => "pbr_events_carousel",
			    "icon" => 'icon-wpb-event-2',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('PBR Event', 'paradise'),
			    "params" => array(
			    	array(
						"type" => "textfield",
						"heading" => esc_html__("Title", 'paradise'),
						"param_name" => "title",
						"value" => '',
						"admin_label" => true
					),
					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Events', 'paradise') => 'featured',
							esc_html__('Lastest Events', 'paradise') => 'most_recent',
							esc_html__('Randown Events', 'paradise') => 'random'
						)
				    ),
					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Columns', 'paradise' ),
						"param_name" => "columns_count",
						"value" => $columns
				    ),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));
		}
	}	

	/**
	  * Register Woocommerce Vendor which will register list of shortcodes
	  */
	function paradise_fnc_init_vc_event_vendor(){
		$vendor = new Paradise_VC_Event();
		add_action( 'vc_after_set_mode', array(
			$vendor,
			'load'
		) );

	}
	add_action( 'after_setup_theme', 'paradise_fnc_init_vc_event_vendor' , 9 );

	class WPBakeryShortCode_pbr_events extends WPBakeryShortCode {}
	class WPBakeryShortCode_pbr_events_carousel extends WPBakeryShortCode {}
}		
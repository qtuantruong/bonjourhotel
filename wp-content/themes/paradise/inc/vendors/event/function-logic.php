<?php

function paradise_fnc_event_query( $type, $number = -1 ) {
	switch ( $type ) {
    	case 'most_recent' : 
	       $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	            'post_type' => 'tribe_events'
	        );
	        break;

	    case 'featured' :
	        $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	            'post_type' => 'tribe_events',
	            'meta_query' => array(
	                    array(
	                        'key'     => 'wpo_postconfig',
	                        'value' => '"is_featured";s:1:"1"',
	                        'compare' => 'like'
	                    ),
	                ),
	            );  
	        break;
	    case 'random' : 
	        $args = array(
	            'post_type' => 'tribe_events',
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	    default : 
	     	$args = array(
	            'post_type' => 'tribe_events',
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	}
	$wp_query = new WP_Query( $args );
	return $wp_query;
}

// breadcrumb
function paradise_event_breadcrumbs() {
	global $post;
	
	$disable = paradise_fnc_theme_options( 'event-show-breadcrumb', 1 );
	if( !$disable ){
		return true; 
	}
	$bgimage = paradise_fnc_theme_options( 'event-breadcrumb' );
	$style = array();

	if( $bgimage  ){ 
		$style[] = 'background-image:url(\''.$bgimage.'\')';
	}
	$estyle = !empty($style)? 'style="'.implode(";", $style).'"':"";
	
	
	echo '<section id="pbr-breadscrumb" class="pbr-breadscrumb" '.$estyle.'><div class="container">';
		paradise_fnc_pagetitle();
	echo '</div></section>';
	echo '<section class="pbr-breadscrumb-inner"><div class="container">';		
		paradise_fnc_breadcrumbs();
	echo '</div></section>';
}
add_action('paradise_event_template_main_before', 'paradise_event_breadcrumbs');

function paradise_fnc_get_event_sidebar_configs( $configs = '' ) {

	$layout =  paradise_fnc_theme_options( 'event-layout', 'fullwidth');
	$left  	=  paradise_fnc_theme_options( 'event-left-sidebar', 'event-sidebar-left');
	$right 	=  paradise_fnc_theme_options( 'event-right-sidebar', 'event-sidebar-right');

	return paradise_fnc_get_layout_configs($layout, $left, $right, $configs );
}
add_action('paradise_fnc_get_event_sidebar_configs', 'paradise_fnc_get_event_sidebar_configs');
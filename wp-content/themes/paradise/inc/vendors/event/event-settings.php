<?php


/**
 * Course Setting
 *
 */
add_action( 'customize_register', 'paradise_fnc_event_settings' );
function paradise_fnc_event_settings( $wp_customize ){
    
    $wp_customize->add_section( 'event_settings', array(
        'priority' => 11,
        'capability' => 'edit_theme_options',
        'theme_supports' => '',
        'title' => esc_html__( 'Event Setting', 'paradise' ),
        'description' => '',
    ) );

    // breadcrumbs
    $wp_customize->add_setting('pbr_theme_options[event-show-breadcrumb]', array(
        'capability' => 'edit_theme_options',
        'type'       => 'option',
        'default'   => 0,
        'checked' => 1,
        'sanitize_callback' => 'sanitize_text_field'
    ) );

    $wp_customize->add_control('pbr_theme_options[event-show-breadcrumb]', array(
        'settings'  => 'pbr_theme_options[event-show-breadcrumb]',
        'label'     => esc_html__('Show breadcrumb', 'paradise'),
        'section'   => 'event_settings',
        'type'      => 'checkbox',
        'transport' => 4,
    ) );

    $wp_customize->add_setting('pbr_theme_options[event-breadcrumb]', array(
        'default'    => '',
        'type'       => 'option',
        'capability' => 'manage_options',
        'sanitize_callback' => 'esc_url_raw',
    ) );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pbr_theme_options[event-breadcrumb]', array(
        'label'    => esc_html__('Breadcrumb background', 'paradise'),
        'section'  => 'event_settings',
        'settings' => 'pbr_theme_options[event-breadcrumb]',
        'priority' => 10,
    ) ) );

}
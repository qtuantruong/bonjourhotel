<?php 
if( class_exists("WPBakeryShortCode") ){
	/**
	 * Class Paradise_VC_Woocommerces
	 *
	 */
	class Paradise_VC_OpalHotel  implements Vc_Vendor_Interface  {

		public function load() {
			$columns = array(1, 2, 3, 4, 6);

			$allpages = array( esc_html__( 'Choose a page', 'paradise' ) => '' );
			if ( is_admin() ) {
				$args = array(
					'sort_order' => 'desc',
					'sort_column' => 'date',
					'post_type' => 'page',
					'post_status' => 'publish'
				); 
				$pages = get_pages($args);
				if ( !empty($pages) ) {
					foreach ($pages as $page) {
						$allpages[$page->post_title] = $page->post_name;
					}
				}
			}

		    vc_map( array(
			    "name" => esc_html__("Grid Rooms",'paradise'),
			    "base" => "opalhotel_grid_rooms",
			    "icon" => 'icon-wpb-hotel-1',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('Hotel', 'paradise'),
			    "params" => array(

				    array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns", 'paradise'),
						"param_name" => "columns",
						"value" => array(6, 4, 3, 2, 1),
					),


					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Rooms', 'paradise') => 'featured',
							esc_html__('Lastest Rooms', 'paradise') => 'most_recent',
							esc_html__('Random Rooms', 'paradise') => 'random'
						)
				    ),

					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Enable Pagination', 'paradise' ),
						'param_name' => 'pagination',
						'value' => array( 'No'=>'0', 'Yes'=>'1'),
						'std' => '1',
						'admin_label' => true,
						'description' => esc_html__( 'Show paginaion.', 'paradise' )
					),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));

			vc_map( array(
			    "name" => esc_html__("List Rooms",'paradise'),
			    "base" => "opalhotel_list_rooms",
			    "icon" => 'icon-wpb-hotel-2',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('Hotel', 'paradise'),
			    "params" => array(

					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Rooms', 'paradise') => 'featured',
							esc_html__('Lastest Rooms', 'paradise') => 'most_recent',
							esc_html__('Random Rooms', 'paradise') => 'random'
						)
				    ),

					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						'type' => 'dropdown',
						'heading' => esc_html__( 'Enable Pagination', 'paradise' ),
						'param_name' => 'pagination',
						'value' => array( 'No'=>'0', 'Yes'=>'1'),
						'std' => 'style-1',
						'admin_label' => true,
						'description' => esc_html__( 'Show paginaion.', 'paradise' )
					),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));

			vc_map( array(
			    "name" => esc_html__("Special List Rooms",'paradise'),
			    "icon" => 'icon-wpb-hotel-3',
                "base" => "opalhotel_overlap_rooms",
                "description" =>'Display special list rooms',
			    "class" => "",
			    "category" => esc_html__('Hotel', 'paradise'),
			    "params" => array(

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns", 'paradise'),
						"param_name" => "columns",
						"value" => array(6, 4, 3, 2, 1),
					),

					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Rooms', 'paradise') => 'featured',
							esc_html__('Lastest Rooms', 'paradise') => 'most_recent',
							esc_html__('Random Rooms', 'paradise') => 'random'
						)
				    ),

					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),

				    array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Style', 'paradise' ),
						"param_name" => "style",
						"value" => array(
							esc_html__('Default', 'paradise') => 'default',
							esc_html__('Style 2', 'paradise') => 'style-2'
						)
				    ),

				    array(
						"type" => "dropdown",
						"heading" => esc_html__("Reservation page",'paradise'),
						"param_name" => "reservation_page",
						"value" => $allpages,
						"admin_label" => true,
						"description" => esc_html__("Select a page if you want to show reservation button.",'paradise')
					),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));

			vc_map( array(
			    "name" => esc_html__("Carousel Rooms",'paradise'),
			    "base" => "opalhotel_carousel_rooms",
			    "icon" => 'icon-wpb-hotel-4',
			    "description" =>'Display carousel list rooms',
			    "class" => "",
			    "category" => esc_html__('Hotel', 'paradise'),
			    "params" => array(

					array(
						"type" => "dropdown",
						"heading" => esc_html__("Columns", 'paradise'),
						"param_name" => "columns",
						"value" => array(6, 5, 4, 3, 2, 1),
					),

					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Order By', 'paradise' ),
						"param_name" => "orderby",
						"value" => array(
							esc_html__('Featured Rooms', 'paradise') => 'featured',
							esc_html__('Lastest Rooms', 'paradise') => 'most_recent',
							esc_html__('Random Rooms', 'paradise') => 'random'
						)
				    ),

					array(
						"type" => "textfield",
						'heading' => esc_html__( 'Number', 'paradise' ),
						"param_name" => "number",
						"value" => ''
				    ),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					)
			   	)
			));

			vc_map( array(
			    "name" => esc_html__("Reservation Form",'paradise'),
			    "base" => "opalhotel_reservation_form",
			    "icon" => 'icon-wpb-hotel-5',
			    "description" =>'Display Event on frontend',
			    "class" => "",
			    "category" => esc_html__('Hotel', 'paradise'),
			    "params" => array(
			     	
					array(
						"type" => "dropdown",
						'heading' => esc_html__( 'Layout', 'paradise' ),
						"param_name" => "layout",
						"admin_label"=>true,
						"value" => array(
							esc_html__('Vertical Form', 'paradise') => '',
							esc_html__('Horizontal Form', 'paradise') => 'horizontal',
						 
						)
				    ),
				    array(
						"type" => "textfield",
						"heading" => esc_html__("Extra class name", 'paradise'),
						"param_name" => "el_class",
						"description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
					),
					array(
						'type' => 'checkbox',
						'heading' => __( 'Show Children', 'paradise' ),
						'param_name' => 'display_childrens',
						'value' => array( __( 'Yes', 'paradise' ) => 'yes' )
					)
			   	)
			));

            vc_map( array(
                "name" => esc_html__("Rooms Main Slider",'paradise'),
                "base" => "opalhotel_slider_rooms",
                "icon" => 'icon-wpb-hotel-4',
                "description" =>'Display rooms full slider',
                "class" => "",
                "category" => esc_html__('Hotel', 'paradise'),
                "params" => array(

                    array(
                        "type" => "dropdown",
                        'heading' => esc_html__( 'Order By', 'paradise' ),
                        "param_name" => "orderby",
                        "value" => array(
                            esc_html__('Featured Rooms', 'paradise') => 'featured',
                            esc_html__('Lastest Rooms', 'paradise') => 'most_recent',
                            esc_html__('Random Rooms', 'paradise') => 'random'
                        )
                    ),

                    array(
                        "type" => "textfield",
                        'heading' => esc_html__( 'Number', 'paradise' ),
                        "param_name" => "number",
                        "value" => ''
                    ),
                    array(
                        "type" => "textfield",
                        "heading" => esc_html__("Extra class name", 'paradise'),
                        "param_name" => "el_class",
                        "description" => esc_html__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'paradise')
                    )
                )
            ));
			
		}
	}	

	/**
	  * Register Woocommerce Vendor which will register list of shortcodes
	  */
	function paradise_fnc_init_vc_opalhotel_vendor(){
		$vendor = new Paradise_VC_OpalHotel();
		add_action( 'vc_after_set_mode', array(
			$vendor,
			'load'
		) );

	}
	add_action( 'after_setup_theme', 'paradise_fnc_init_vc_opalhotel_vendor' , 9 );

	class WPBakeryShortCode_Opalhotel_grid_rooms extends WPBakeryShortCode {}
	class WPBakeryShortCode_Opalhotel_list_rooms extends WPBakeryShortCode {}
	class WPBakeryShortCode_Opalhotel_overlap_rooms extends WPBakeryShortCode {}
	class WPBakeryShortCode_Opalhotel_carousel_rooms extends WPBakeryShortCode {}
	class WPBakeryShortCode_Opalhotel_slider_rooms extends WPBakeryShortCode {}
	class WPBakeryShortCode_Opalhotel_reservation_form extends WPBakeryShortCode {}
}		
<?php  
 
function paradise_opalhotel_template_path(){
    return '/opalhotel/';
}
add_filter( 'opalhotel_template_path', 'paradise_opalhotel_template_path' );

function paradise_fnc_get_single_room_sidebar_configs( $configs='' ){

    global $post; 

    $layout =  paradise_fnc_theme_options( 'opalhotel-single-layout', 'fullwidth');
    $left   =  paradise_fnc_theme_options( 'opalhotel-single-left-sidebar', 'opalhotel-sidebar-left');
    $right  =  paradise_fnc_theme_options( 'opalhotel-single-right-sidebar', 'opalhotel-sidebar-right');

 
    
    return paradise_fnc_get_layout_configs($layout, $left, $right, $configs);
}
add_filter( 'paradise_fnc_get_single_room_sidebar_configs', 'paradise_fnc_get_single_room_sidebar_configs', 1, 1 );

function paradise_fnc_get_archive_room_sidebar_configs( $configs='' ){

    $layout =  paradise_fnc_theme_options( 'opalhotel-archive-layout', 'fullwidth');

    $left  =  paradise_fnc_theme_options( 'opalhotel-archive-left-sidebar' ); 

    $right =  paradise_fnc_theme_options( 'opalhotel-archive-right-sidebar' );

    return paradise_fnc_get_layout_configs( $layout, $left, $right, $configs );
}

add_filter( 'paradise_fnc_get_archive_room_sidebar_configs', 'paradise_fnc_get_archive_room_sidebar_configs', 1, 1 );

/**
 *
 *
 */
function paradise_opalhotel_room_query( $type='', $number=4, $paged=1 ){
	switch ( $type ) {
    	case 'most_recent' : 
	       $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	         
	        );
	        break;

	    case 'featured' :
	        $args = array( 
	            'posts_per_page' => $number, 
	            'orderby' => 'date', 
	            'order' => 'DESC',
	          
	            'meta_query' => array(
	                    array(
	                        'key'     => 'wpo_postconfig',
	                        'value' => '"is_featured";s:1:"1"',
	                        'compare' => 'like'
	                    ),
	                ),
	            );  
	        break;
	    case 'random' : 
	        $args = array(
	            'post_type' => 'opalhotel_room',
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	    default : 
	     	$args = array(
	          
	            'posts_per_page' => $number, 
	            'orderby' => 'rand'
	        );
	        break;
	}

	$args['post_type'] =  'opalhotel_room';
    $args['paged'] = $paged;
	$wp_query = new WP_Query( $args );

	wp_reset_query();
	
	return $wp_query;
}

/**
 * Course Setting
 *
 */
add_action( 'customize_register', 'paradise_fnc_opalhotel_settings' );

function paradise_fnc_opalhotel_settings( $wp_customize ){

		$wp_customize->add_panel( 'opalhotel_settings', array(
    		'priority' => 70,
    		'capability' => 'edit_theme_options',
    		'theme_supports' => '',
    		'title' => esc_html__( 'OpalHotel Setting', 'paradise' ),
    		'description' =>esc_html__( 'Make default setting for page, general', 'paradise' ),
    	) );

     
        /**
         * Archive Page Setting
         */
        $wp_customize->add_section( 'opalhotel_archive_settings', array(
            'priority' => 2,
            'capability' => 'edit_theme_options',
            'theme_supports' => '',
            'title' => esc_html__( 'Archive Page Setting', 'paradise' ),
            'description' => 'Configure categories page setting',
            'panel' => 'opalhotel_settings',
        ) );

         ///  Archive layout setting
        $wp_customize->add_setting( 'pbr_theme_options[opalhotel-archive-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Paradise_Layout_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-archive-layout]', array(
            'settings'  => 'pbr_theme_options[opalhotel-archive-layout]',
            'label'     => esc_html__('Archive Layout', 'paradise'),
            'section'   => 'opalhotel_archive_settings',
            'priority' => 1

        ) ) );

       //sidebar archive left
        $wp_customize->add_setting( 'pbr_theme_options[opalhotel-archive-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-left',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Paradise_Sidebar_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-archive-left-sidebar]', array(
            'settings'  => 'pbr_theme_options[opalhotel-archive-left-sidebar]',
            'label'     => esc_html__('Archive Left Sidebar', 'paradise'),
            'section'   => 'opalhotel_archive_settings' ,
             'priority' => 3
        ) ) );

          //sidebar archive right
        $wp_customize->add_setting( 'pbr_theme_options[opalhotel-archive-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'checked' => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        $wp_customize->add_control( new Paradise_Sidebar_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-archive-right-sidebar]', array(
            'settings'  => 'pbr_theme_options[opalhotel-archive-right-sidebar]',
            'label'     => esc_html__('Archive Right Sidebar', 'paradise'),
            'section'   => 'opalhotel_archive_settings',
             'priority' => 4 
        ) ) );

    	$wp_customize->add_setting('pbr_theme_options[opalhotel-archive-breadcrumb]', array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'manage_options',
            'sanitize_callback' => 'esc_url_raw',
        ) );

        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pbr_theme_options[opalhotel-archive-breadcrumb]', array(
            'label'    => esc_html__('Breadcrumb Background', 'paradise'),
            'section'  => 'opalhotel_archive_settings',
            'settings' => 'pbr_theme_options[opalhotel-archive-breadcrumb]',
            'priority' => 12,
        ) ) );

        /**
    	 * Room Single Setting
    	 */
    	$wp_customize->add_section( 'opalhotel_room_settings', array(
    		'priority' => 12,
    		'capability' => 'edit_theme_options',
    		'theme_supports' => '',
    		'title' => esc_html__( 'Single Room Page Setting', 'paradise' ),
    		'description' => 'Configure single room page',
    		'panel' => 'opalhotel_settings',
    	) );
        ///  single layout setting
        $wp_customize->add_setting( 'pbr_theme_options[opalhotel-single-layout]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'mainright',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Select layout
        $wp_customize->add_control( new Paradise_Layout_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-single-layout]', array(
            'settings'  => 'pbr_theme_options[opalhotel-single-layout]',
            'label'     => esc_html__('Room Detail Layout', 'paradise'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 1
        ) ) );

       
        $wp_customize->add_setting( 'pbr_theme_options[opalhotel-single-left-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 1,
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar left
        $wp_customize->add_control( new Paradise_Sidebar_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-single-left-sidebar]', array(
            'settings'  => 'pbr_theme_options[opalhotel-single-left-sidebar]',
            'label'     => esc_html__('Room Detail Left Sidebar', 'paradise'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 2 
        ) ) );

         $wp_customize->add_setting( 'pbr_theme_options[opalhotel-single-right-sidebar]', array(
            'capability' => 'edit_theme_options',
            'type'       => 'option',
            'default'   => 'sidebar-right',
            'sanitize_callback' => 'sanitize_text_field'
        ) );

        //Sidebar right
        $wp_customize->add_control( new Paradise_Sidebar_DropDown( $wp_customize,  'pbr_theme_options[opalhotel-single-right-sidebar]', array(
            'settings'  => 'pbr_theme_options[opalhotel-single-right-sidebar]',
            'label'     => esc_html__('Room Detail Right Sidebar', 'paradise'),
            'section'   => 'opalhotel_room_settings',
            'priority' => 3 
        ) ) );

        $wp_customize->add_setting('pbr_theme_options[opalhotel-single-breadcrumb]', array(
            'default'    => '',
            'type'       => 'option',
            'capability' => 'manage_options',
            'sanitize_callback' => 'esc_url_raw',
        ) );

        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'pbr_theme_options[opalhotel-single-breadcrumb]', array(
            'label'    => esc_html__('Breadcrumb Background', 'paradise'),
            'section'  => 'opalhotel_room_settings',
            'settings' => 'pbr_theme_options[opalhotel-single-breadcrumb]',
            'priority' => 12,
        ) ) );

	}
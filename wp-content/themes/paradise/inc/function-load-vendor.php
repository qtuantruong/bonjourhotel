<?php 

/**
 * Check and load to support visual composer
 */
if(  in_array( 'js_composer/js_composer.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && class_exists('WPBakeryVisualComposerAbstract') ){
	paradise_pbr_includes(  get_template_directory() . '/inc/vendors/visualcomposer/*.php' );
}

/**
 * Check and load to support visual composer
 */
if(  in_array( 'opalhotel/opalhotel.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	paradise_pbr_includes(  get_template_directory() . '/inc/vendors/opalhotel/*.php' );
}

/**
 * Check and load to support visual composer
 */
if( in_array( 'the-events-calendar/the-events-calendar.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
	paradise_pbr_includes(  get_template_directory() . '/inc/vendors/event/*.php' );
}

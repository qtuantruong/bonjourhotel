<?php
/**
 * The template for displaying posts in the Video post format
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
$videolink =  get_post_meta( get_the_ID(),'paradise_video_link', true );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-content-wrapper">

		<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && paradise_fnc_categorized_blog() ) : ?>
			<?php
			endif;

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
			endif;
		?>

		<div class="post-sub-content clearfix">
			<div class="entry-date pull-left">
	            <span><?php the_time( 'd' ); ?></span>&nbsp;<?php the_time( 'M' ); ?>
	        </div>
	        <div class="entry-category pull-left">
                <?php esc_html_e('in', 'paradise'); the_category(); ?>
            </div>
			<span class="author pull-left"><?php esc_html_e('By', 'paradise'); ?><?php the_author_posts_link(); ?></span>
		</div>
		
	</header><!-- .entry-header -->

	<div class="post-preview">
		<?php if( $videolink ) : ?>
			<div class="video-thumb video-responsive"><?php echo wp_oembed_get( $videolink ); ?></div>
		<?php else : ?>
			<?php paradise_fnc_post_thumbnail(); ?>
		<?php endif; ?>
	
		<span class="post-format hide">
			<a class="entry-format" href="<?php echo esc_url( get_post_format_link( 'image' ) ); ?>"><i class="fa fa-film"></i></a>
		</span>
	</div>

	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				wp_kses_post( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'paradise' ) ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'paradise' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<div class="clearfix">
			<?php the_tags( '<div class="pull-left"><span class="tag-links">', '', '</span></div>' ); ?>
			<div class="pull-right">
				<?php
					if ( paradise_fnc_theme_options( 'blog-show-share-post', 1 ) ) {
						get_template_part( 'page-templates/parts/sharebox' );
					}
				?>
			</div>
		</div>		
	</footer>

	
</article><!-- #post-## -->

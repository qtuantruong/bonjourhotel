<?php
/**
 * $Desc
 *
 * @version    $Id$
 * @package    wpbase
 * @author     WpOpal Team <help@wpopal.com, info@wpopal.com>
 * @copyright  Copyright (C) 2015 wpopal.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @website  http://www.wpopal.com
 * @support  http://www.wpopal.com/questions/
 */
if( $title )
	echo ($before_title)  . trim( $title ) . $after_title;
?>
<div class="bo-social-icons bo-sicolor text-center">
    <?php foreach( $socials as $key=>$social):
            if( isset($social['status']) && !empty($social['page_url']) ): ?>
                <a href="<?php echo esc_url($social['page_url']);?>" class="bo-social-<?php echo esc_attr($key); ?>">
                    <i class="fa fa-<?php echo esc_attr($key); ?>">&nbsp;</i>
                </a>
    <?php
            endif;
        endforeach;
    ?>
</div>

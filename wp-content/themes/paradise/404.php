<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WpOpal
 * @subpackage paradise
 * @since paradise 1.0
 */
/*
*Template Name: 404 Page
*/

get_header( apply_filters( 'paradise_fnc_get_header_layout', null ) ); ?>

<?php do_action( 'paradise_template_main_before' ); ?>
<section id="main-container" class="<?php echo apply_filters('paradise_template_main_container_class','container');?> inner notfound-page">
	<div class="row">
		<div id="main-content" class="main-content col-lg-12">
			<div id="primary" class="content-area">
				 <div id="content" class="site-content" role="main">

					<div class="row error-404-content">
						<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
							<?php if( paradise_fnc_theme_options('error-404-bg') ):  ?>

							<img class="error-404-bg" src="<?php echo paradise_fnc_theme_options('error-404-bg'); ?>" alt="<?php bloginfo( 'name' ); ?>" />

							<?php else: ?>

							<img class="error-404-bg" src="<?php echo get_template_directory_uri() . '/images/background-09.png'; ?>" alt="<?php bloginfo( 'name' ); ?>" />

							<?php endif; ?>
						</div>
						<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
							<div class="page-content text-center">
								<h2><?php esc_html_e( 'Oops, that link is broken.', 'paradise' ); ?></h2>
								<p><?php esc_html_e( 'Page does not exist or some other error occured. Go to our', 'paradise' ); ?></p>
								 
							</div><!-- .page-content -->

							<div class="page-action text-center">						
								<a class="text-primary" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home page', 'paradise'); ?></a>
								<span><?php esc_html_e('or go back to', 'paradise'); ?></span>
								<a class="text-primary" href="javascript: history.go(-1)"><?php esc_html_e('Previous page', 'paradise'); ?></a>
							</div>
						</div>
					</div>

				</div><!-- #content -->
			</div><!-- #primary -->
			<?php get_sidebar( 'content' ); ?>
		</div><!-- #main-content -->

		 
		<?php get_sidebar(); ?>
	 
	</div>	
</section>
<?php

get_footer();

 
<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

global $post;
$start = strtotime($post->EventStartDate);
$day = date('d', $start);
$month = date('M', $start);

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();
// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';
// Organizer
$organizer = tribe_get_organizer();
?>
<div class="tribe-events-list">

  <div class="row">

    <div class="col-md-4">
      <?php echo tribe_event_featured_image( null, 'full' ) ?>
    </div>

  	<div class="col-md-5">

      <div class="tribe-events-inner">
    		<!-- Event Title -->
    		<?php do_action( 'tribe_events_before_the_event_title' ) ?>
    		<h2 class="tribe-events-list-event-title">
    			<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
    				<?php the_title() ?>
    			</a>
    		</h2>
        <!-- Event Cost -->
        <?php if ( tribe_get_cost() ) : ?>
          <div class="tribe-events-event-cost">
            <span><?php echo tribe_get_cost( null, true ); ?></span>
          </div>
        <?php endif; ?>
    		<?php do_action( 'tribe_events_after_the_event_title' ) ?>

    		

    		<!-- Event Content -->
    		<?php do_action( 'tribe_events_before_the_content' ) ?>
    		<div class="tribe-events-list-event-description tribe-events-content">
    			<?php echo tribe_events_get_the_excerpt(); ?>
    		</div><!-- .tribe-events-list-event-description -->
    		<?php
    		do_action( 'tribe_events_after_the_content' );
    		?>
      </div>
  	</div>

    <div class="col-md-3">
      <!-- Event Meta -->
      <?php do_action( 'tribe_events_before_the_meta' ) ?>

      <div class="tribe-events-event-meta">
        <div class="<?php echo esc_attr( $has_venue_address ); ?>">

          <div class="event-time-wrapper">
            <i class="fa fa-clock-o"></i>
            <span class="event-time">
              <?php
                  echo tribe_get_start_date(get_the_ID(), false, 'H:i');
              ?>
              <span>-</span>
              <?php
                  if(tribe_get_end_date(get_the_ID(), false, 'dMY') == tribe_get_start_date(get_the_ID(), false, 'dMY')){
                      echo tribe_get_end_date(get_the_ID(), false, 'H:i');
                  }else{
                      echo tribe_get_end_date(get_the_ID(), false, 'M, d H:i');
                  }
              ?>
            </span>
          </div>
          
          <!-- Venue Display Info -->
          <div class="event-address-wrapper">
            
            <?php if ( $venue_details ) : ?>
              <!-- Venue Display Info -->
              <div class="tribe-events-venue-details">
                <?php echo implode( ', ', $venue_details ); ?>
              </div> <!-- .tribe-events-venue-details -->
            <?php endif; ?>
          </div>
          
        </div>
      </div>

      <?php do_action( 'tribe_events_after_the_meta' ) ?>
    </div>
  	
  </div>
</div>
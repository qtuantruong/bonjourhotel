<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

global $post;
$start = strtotime($post->EventStartDate);
$day = date('d', $start);
$month = date('M', $start);

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();
// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';
// Organizer
$organizer = tribe_get_organizer();
?>
<div class="tribe-events-item carousel-item clearfix">
	<div class="row">
		
		<div class="col-md-6">
			<?php echo tribe_event_featured_image( null, 'full' ) ?>
		</div>

		<div class="col-md-6 clearfix">
			
			<div class="tribe-events-inner text-center">
				<div class="tribe_event_category">
					<?php
					echo tribe_get_event_categories(
						get_the_id(), array(
							'before'       => '',
							'sep'          => ', ',
							'after'        => '',
							'label'        => '', // An appropriate plural/singular label will be provided
							'label_before' => '',
							'label_after'  => '',
							'wrap_before'  => '<span class="tribe-events-span">',
							'wrap_after'   => '</span>'
						)
					);
					?>
				</div>
				<!-- Event Title -->
				<?php do_action( 'tribe_events_before_the_event_title' ) ?>
				<h2 class="tribe-events-list-event-title">
					<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
						<?php the_title() ?>
					</a>
				</h2>
				<?php do_action( 'tribe_events_after_the_event_title' ) ?>
				<div class="tribe-events-list-event-description tribe-events-content">
	    			<?php echo tribe_events_get_the_excerpt(); ?>
	    		</div><!-- .tribe-events-list-event-description -->
	    		
			</div>
		</div>
		
	</div>
</div>
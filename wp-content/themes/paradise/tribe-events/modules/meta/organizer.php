<?php
/**
 * Single Event Meta (Organizer) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/details.php
 *
 * @package TribeEventsCalendar
 */

$phone = tribe_get_organizer_phone();
$email = tribe_get_organizer_email();
$website = tribe_get_organizer_website_link();
?>

<div class="tribe-events-meta-group tribe-events-meta-group-organizer widget">
	<h3 class="widget-title"> <span><?php echo tribe_get_organizer_label_singular(); ?></span> </h3>
	<ul class="meta-details">
		<?php do_action( 'tribe_events_single_meta_organizer_section_start' ) ?>
		<?php if ( ! empty( $phone ) ): ?>
			<li class="tel"> 
				<?php esc_html_e( 'Phone:', 'paradise' ) ?>
				<span class="tribe-events-span"><?php echo esc_html( $phone ); ?></span>	
			</li>
		<?php endif ?>

		<?php if ( ! empty( $website ) ): ?>
			<li class="url"> 
				<?php esc_html_e( 'Website:', 'paradise' ) ?>
				<span class="tribe-events-span">
					<?php echo trim( $website ); ?> 
				</span>
			</li>
		<?php endif ?>
		<?php do_action( 'tribe_events_single_meta_organizer_section_end' ) ?>
	</ul>
</div>
<?php
/**
 * Single Event Meta (Details) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/details.php
 *
 * @package TribeEventsCalendar
 */
?>

<div class="tribe-events-meta-group tribe-events-meta-group-details widget">
	<h3 class="widget-title"> <span><?php esc_html_e( 'Details', 'paradise' ) ?></span> </h3>
	<ul class="meta-details">

		<?php
		do_action( 'tribe_events_single_meta_details_section_start' );

		$time_format = get_option( 'time_format', Tribe__Date_Utils::TIMEFORMAT );
		$time_range_separator = tribe_get_option( 'timeRangeSeparator', ' - ' );

		$start_datetime = tribe_get_start_date();
		$start_date = tribe_get_start_date( null, false );
		$start_time = tribe_get_start_date( null, false, $time_format );
		$start_ts = tribe_get_start_date( null, false, Tribe__Date_Utils::DBDATEFORMAT );

		$end_datetime = tribe_get_end_date();
		$end_date = tribe_get_end_date( null, false );
		$end_time = tribe_get_end_date( null, false, $time_format );
		$end_ts = tribe_get_end_date( null, false, Tribe__Date_Utils::DBDATEFORMAT );

		// All day (multiday) events
		if ( tribe_event_is_all_day() && tribe_event_is_multiday() ) :
			?>

			<li>
				<?php esc_html_e( 'Start:', 'paradise' ) ?>
				<span class="tribe-events-span updated published listart" title="<?php echo esc_attr( $start_ts ) ?>"> <?php echo esc_html( $start_date ) ?> </span>
			</li>
			<li>
				<?php esc_html_e( 'End:', 'paradise' ) ?>
				<span class="tribe-events-span liend" title="<?php echo esc_attr( $end_ts ) ?>"> <?php echo esc_html( $end_date ) ?> </span>
			</li>

		<?php
		// All day (single day) events
		elseif ( tribe_event_is_all_day() ):
			?>
			<li>
				<?php esc_html_e( 'Date:', 'paradise' ) ?>
				<span class="tribe-events-span updated published listart" title="<?php echo esc_attr( $start_ts ) ?>"> <?php echo esc_html( $start_date ) ?> </span>
			</li>
		<?php
		// Multiday events
		elseif ( tribe_event_is_multiday() ) :
			?>

			<li>
				<?php esc_html_e( 'Start:', 'paradise' ) ?>
				<span class="tribe-events-span updated published listart" title="<?php echo esc_attr( $start_ts ) ?>"> <?php echo esc_html( $start_datetime ) ?> </span>
			</li>
			<li>
				<?php esc_html_e( 'End:', 'paradise' ) ?>
				<span class="tribe-events-span liend" title="<?php echo esc_attr( $end_ts ) ?>"> <?php echo esc_html( $end_datetime ) ?> </span>
			</li>
		<?php
		// Single day events
		else :
			?>
			<li>
				<?php echo esc_html( 'Date:', 'paradise' ) ?>
				<span class="tribe-events-span updated published listart" title="<?php echo esc_attr( $start_ts ) ?>"> <?php echo esc_html( $start_date ) ?> </span>
			</li>
			<li>
			<?php esc_html_e( 'Time:', 'paradise' ) ?>
			<span class="tribe-events-span updated published listart" title="<?php echo esc_attr( $end_ts ) ?>">
					<?php if ( $start_time == $end_time ) {
						echo esc_html( $start_time );
					} else {
						echo esc_html( $start_time . $time_range_separator . $end_time );
					} ?>
				</span></li>
		<?php endif ?>
		<li>
			<?php
			echo tribe_get_event_categories(
				get_the_id(), array(
					'before'       => '',
					'sep'          => ', ',
					'after'        => '',
					'label'        => null, // An appropriate plural/singular label will be provided
					'label_before' => '',
					'label_after'  => '',
					'wrap_before'  => '<span class="tribe-events-span">',
					'wrap_after'   => '</span>'
				)
			);
			?>
		</li>
		<?php do_action( 'tribe_events_single_meta_details_section_end' ) ?>
	</ul>
</div>
<?php
/**
 * Single Event Meta (Venue) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/venue.php
 *
 * @package TribeEventsCalendar
 */

if ( ! tribe_get_venue_id() ) {
	return;
}
$phone = tribe_get_phone();
$website = tribe_get_venue_website_link();
?>

<div class="tribe-events-meta-group tribe-events-meta-group-venue">
	<h3 class="widget-title"> <span><?php echo tribe_get_venue_label_singular(); ?></span> </h3>
	<div class="meta-group-venue">
		<?php
			$address = tribe_address_exists() ? '<address class="tribe-events-address">' . tribe_get_full_address() . '</address>' : '';

			// Do we have a Google Map link to display?
			$gmap_link = tribe_show_google_map_link() ? tribe_get_map_link_html() : '';
			$gmap_link = apply_filters( 'tribe_event_meta_venue_address_gmap', $gmap_link );
			// Display if appropriate
			if ( ! empty( $address ) ) {
				echo '<div class="location">' . "$address $gmap_link </div>";
			}
		?>
		<ul class="meta-details">
			<?php do_action( 'tribe_events_single_meta_venue_section_start' ) ?>

			<li class="venue">
				<?php esc_html_e( 'Venue:', 'paradise' ) ?>
				<span class="tribe-events-span"><?php echo tribe_get_venue() ?></span>
			</li>

			<?php if ( ! empty( $phone ) ): ?>
				<li class="tel">
					<?php esc_html_e( 'Phone:', 'paradise' ) ?>
					<span class="tribe-events-span"><?php echo esc_html( $phone ) ?> </span>
				</li>
			<?php endif ?>

			<?php if ( ! empty( $website ) ): ?>
				<li class="url"> 
					<?php esc_html_e( 'Website:', 'paradise' ) ?>
					<span class="tribe-events-span"><?php echo trim( $website ); ?> </span>
				</li>
			<?php endif ?>

			<?php do_action( 'tribe_events_single_meta_venue_section_end' ) ?>
		</ul>
	</div>
</div>
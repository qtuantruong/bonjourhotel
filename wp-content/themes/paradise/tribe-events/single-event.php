<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();
$event_id = get_the_ID();

?>

<?php while ( have_posts() ) :  the_post(); ?>
<div class="clearfix single-event">
	<div class="row">
		
		<div class="col-sm-8">
			<div id="tribe-events-content" class="tribe-events-single">

				<!-- Notices -->
				<?php tribe_the_notices() ?>

				<?php the_title( '<h1 class="tribe-events-single-event-title">', '</h1>' ); ?>
				<div class="tribe-events-schedule tribe-clearfix">
					<?php echo tribe_events_event_schedule_details( $event_id, '<div class="events-meta">', '</div>' ); ?>
					<?php if ( tribe_get_cost() ) : ?>
						<span class="tribe-events-divider">|</span>
						<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span>
					<?php endif; ?>
				</div>

				<!-- Event content -->
				<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
				<div class="tribe-events-single-event-description tribe-events-content entry-content description">
					<div class="image-event">
					<?php the_post_thumbnail('full'); ?>
					</div>
					<div class="clearfix"></div>
					<?php the_content(); ?>
				</div><!-- .tribe-events-single-event-description -->
				<div class="social clearfix">
					<div class="pull-left">
						<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
					</div>
					<div class="pull-right">
						<div class="addthis">
				        <!-- AddThis Button BEGIN -->
				        <div class="addthis_toolbox addthis_default_style">
				         <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
				         <a class="addthis_button_tweet"></a>
				         <a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
				         <a class="addthis_counter addthis_pill_style"></a>
				        </div>
				        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-533e342d186e8c37"></script>
				        <!-- AddThis Button END -->
				       </div>
					</div>	
				</div>

				<?php if( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>

			</div><!-- #tribe-events-content -->
		</div>

		<div class="col-sm-4">
			<div class="wpo-tribe-events-meta sidebar">
				<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
				<?php
				/**
				 * The tribe_events_single_event_meta() function has been deprecated and has been
				 * left in place only to help customers with existing meta factory customizations
				 * to transition: if you are one of those users, please review the new meta templates
				 * and make the switch!
				 */
				if ( ! apply_filters( 'tribe_events_single_event_meta_legacy_mode', false ) ) {
					tribe_get_template_part( 'modules/meta' );
				} else {
					echo tribe_events_single_event_meta();
				}
				?>
				<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
			</div>
		</div>

	</div>
</div>
<?php endwhile; ?>

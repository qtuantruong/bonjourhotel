<?php
/**
 * The Template for displaying all single posts
 *
 * @package WPOPAL
 * @subpackage paradise
 * @since Paradise 1.0
 */

$paradise_page_layouts = apply_filters( 'paradise_fnc_get_event_sidebar_configs', null );
get_header( apply_filters( 'paradise_fnc_get_header_layout', null ) );
?>
<?php do_action( 'paradise_event_template_main_before' ); ?>
<section id="main-container" class="container">
	<div class="row">
		<?php if( isset($paradise_page_layouts['sidebars']) && !empty($paradise_page_layouts['sidebars']) ) : ?>
			<?php get_sidebar(); ?>
		<?php endif; ?>
		<div id="main-content" class="single-blog main-content col-sm-12 <?php echo esc_attr($paradise_page_layouts['main']['class']); ?>">

			<div id="primary" class="content-area">
				<div id="content" class="site-content" role="main">
					
					<?php tribe_events_before_html(); ?>
					<?php tribe_get_view(); ?>
					<?php tribe_events_after_html(); ?>
				</div><!-- #content -->
			</div><!-- #primary -->
		</div>	

	</div>	
</section>
<?php
get_footer();
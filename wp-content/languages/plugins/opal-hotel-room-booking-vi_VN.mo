��    I      d      �      �     �  -   �  T   �     E     V  	   ^     h     o     |     �  ;   �     �     �     �           	  	              '     3     8     E     M     f  >   r  H   �     �     	          #  
   /     :     >  	   Z     d     w  	   {     �     �      �     �     �     �     �     �     �               +     9     B     N  	   [  	   e     o  
        �     �     �     �     �     �  	   �     �     �     �     �     �     �  	   �     �     �  �  		     �
  7   �
  n   �
     f     u     �     �  
   �  	   �     �     �     �     �     	          $     1     :     C     Q     ^  
   j  "   u     �  1   �  F   �     !     -     ?  	   _     i  
   n     y     �     �     �     �     �     �     �     �          #     ?     H  &   ]     �     �     �     �     �     �     �     �     �                    %     3     :     G     N  
   Z     e     q          �     �  	   �  	   �     �   ---Select Country--- <strong>Country</strong> is a required field. <strong>Term and Conditional</strong> is require field. Please accept the our terms. Additional Notes Address Adult: %d Adults Apply Coupon Bed Book This Room Can not make a reservation. You have not selected any room. Check Avaiable Check Availability Check availability Children Children: %d Childrens Childs Choose Room City Confirmation Country Coupon %s is not exists. Coupon Code Coupon code <strong>%s</strong> has been applied successfully. Coupon: %s (<a href="#" class="remove_coupon" data-code="%s">Remove</a>) Discount Price Email Enter your address. Extra Price First Name Fri Have you got a coupon code? Last Name Make A Reservation Mon Next Week Notes. Number of bed. Page TitleTerms and Conditions  Pay on Arrival Payment Method Payment method People Phone Please select number of room. Postcode Price Details Pricing Plans Quantity Reservation Room / Night Room Size Room Type Room not found. Room size. Rooms Sat Select Room Type Subtotal Sun Tax This Week Thu Total Total%s Tue URL slugrooms View View More Wed Your Reservation Project-Id-Version: Opal Hotel Room Booking
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-07-08 01:38+0000
PO-Revision-Date: 2018-01-10 10:13+0000
Last-Translator: admin <ttquoctuan@gmail.com>
Language-Team: Vietnamese (Viet Nam)
Language: vi-VN
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/ ---Chọn quốc gia--- <strong>Quốc gia</strong> là trường bắt buộc. <strong>Điều khoản</strong> là bắc buộc. Vui lòng chấp nhận điều khoản để tiếp tục. Ghi chú thêm Địa chỉ Người lớn: %d Người lớn Áp dụng Giường Đặt phòng này Vui lòng chọn phòng. Kiểm tra phòng Kiểm tra phòng Kiểm tra phòng Trẻ em Trẻ em: %d Trẻ em Trẻ em Chọn phòng Thành phố Xác nhận Quốc gia Mã giảm giá không tồn tại Mã giảm giá Mã giảm giá được áp dụng thành công. Coupon: %s (<a href="#" class="remove_coupon" data-code="%s">Xóa</a>) Giảm giá Thư điện tử Nhập địa chỉ của bạn Phụ thu Họ Thứ sáu Bạn có mã giảm giá? Tên Đặt phòng Thứ 2 Tuần tới Ghi chú Số giường Điều khoản sử dụng Thanh toán khi đến Phương thức thanh toán Phương thức thanh toán Người Số điện thoại Vui lòng chọn số lượng phòng. Mã bưu chính Giá Bảng giá Số lượng Đặt phòng Phòng / Đêm Diện tích Loại phòng Không tìm thấy phòng Diện tích Phòng Thứ bảy Chọn phòng Tổng Chủ nhật Thuế Tuần này Thứ năm Tổng giá Tổng giá%s Thứ ba phòng Hướng Xem thêm Thứ tư Đặt phòng 